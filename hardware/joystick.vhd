library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity joystick is
  -- CLK antags inte vara snabbare �n 100 MHz. Joysticken klarar inte
  -- n�dv�ndigtvis av snabbare klocka �n 1 MHz, och det som vi skickar ut till
  -- den �r en hundradelning av signalen CLK h�r.
  port(clk, led1, led2, rst: in std_logic;
       getbits: in std_logic;
       inbit: in std_logic;
       outbit: out std_logic;
       jstk_clk: out STD_LOGIC;
       slave_select: out STD_LOGIC;
       outvalue: out std_logic_vector(31 downto 0);
       write_enable_regs: out STD_LOGIC);
  
end joystick;


architecture joystick_arc of joystick is
  signal counter: UNSIGNED(3 downto 0);
  signal tmpvalue: std_logic_vector(31 downto 0);
  signal shifter: std_logic;
  
  signal hundred_counter: UNSIGNED(6 downto 0) := "0000000";
  
  type meta_state is (getdata, waiting);
  signal current_meta_state: meta_state;

  type sub_state is (nothing, initial_wait, first_byte, first_wait, second_byte, second_wait, third_byte, third_wait, fourth_byte, fourth_wait, fifth_byte);
  signal current_sub_state: sub_state;
begin

  -----------------------------------------------------------------------------
  -- Styrspaksstandarden:
  -- 1. S�nk SS
  -- 2. V�nta 15 mikrosekunder
  -- 3. Skicka in 8 bitar, tag emot 8 bitar
  -- 4. V�nta 10 mikrosekunder
  -- 5. Upprepa 3->4 fyra g�nger till (f�r totalt 5*8 bitar)
  -- H�j SS
  -----------------------------------------------------------------------------
  
  process(clk)
  begin
    if rising_edge(clk) then

      if hundred_counter >= 50 then
        jstk_clk <= '0';
      end if;
      
      if rst = '1' then
        hundred_counter <= "0000000";
        counter <= "0000";
        tmpvalue <= "00000000000000000000000000000000";
        current_meta_state <= waiting;
        current_sub_state <= nothing;
        slave_select <= '0';
        outbit <= '0';
      else
        -- Se till att hundred_counter g�r 0->100 och r�knar varje snabb klockcykel.
        if hundred_counter < 99 then
          hundred_counter <= hundred_counter + 1;
        else
          hundred_counter <= "0000000";
        end if;
        
        if getbits = '1' and current_meta_state = waiting then
          -- Om vi f�r signalen "getbits" utifr�n i en snabb klockcykel, reagerar
          -- vi genom att byta maskinens tillst�nd till "getdata" om vi inte
          -- redan �r i det tillst�ndet och nollar counter. Data�verf�ringen g�r
          -- nu igenom sina tillst�nd.
          current_meta_state <= getdata;
          current_sub_state <= initial_wait;
          counter <= "0000";
        elsif hundred_counter = "0000000" then
          -- Om getbits inte �r p�, och vi �r p� en l�ngsam klockcykel (1 MHz):
          if current_meta_state = getdata then
            slave_select <= '0';

            -- Vi inleder �verf�ringsl�get med 15 mikrosekunders (= klockcyklers) v�ntan:
            if current_sub_state = initial_wait then
              counter <= counter + 1;
              outbit <= '0';
              if counter >= 15 then
                current_sub_state <= first_byte;
                counter <= "0000";
              end if;
            elsif current_sub_state = first_byte then
              -- De f�rsta �tta bitarna kommer...
              counter <= counter + 1;
              case counter is
                when "0000" => jstk_clk <= '1';
                               tmpvalue(22) <= inbit;
                               outbit <= '1';
                when "0001" => jstk_clk <= '1';
                               tmpvalue(23) <= inbit;
                               outbit <= '0';
                when "0010" => jstk_clk <= '1';
                               tmpvalue(24) <= inbit;
                               outbit <= '0';
                when "0011" => jstk_clk <= '1';
                               tmpvalue(25) <= inbit;
                               outbit <= '0';
                when "0100" => jstk_clk <= '1';
                               tmpvalue(26) <= inbit;
                               outbit <= '0';
                when "0101" => jstk_clk <= '1';
                               tmpvalue(27) <= inbit;
                               outbit <= '0';
                when "0110" => jstk_clk <= '1';
                               tmpvalue(28) <= inbit;
                               outbit <= led2;
                when "0111" => jstk_clk <= '1';
                               tmpvalue(29) <= inbit;
                               outbit <= led1;
                               counter <= "0000";
                               current_sub_state <= first_wait;
                when others => counter <= "0000";
                               current_sub_state <= nothing;
              end case;
            elsif current_sub_state = first_wait then
              -- Vi v�ntar tio klockcykler mellan f�rsta och andra byten.
              counter <= counter + 1;
              outbit <= '0';
              if counter >= 10 then
                current_sub_state <= second_byte;
                counter <= "0000";
              end if;
            elsif current_sub_state = second_byte then
              -- �tta bitar till kommer... vi vill ha de tv� f�rsta men ignorerar
              -- de sista sex.
              counter <= counter + 1;
              case counter is
                when "0000" => jstk_clk <= '1';
                               outbit <= '1';
                when "0001" => jstk_clk <= '1';
                               outbit <= '0';
                when "0010" => jstk_clk <= '1';
                               outbit <= '0';
                when "0011" => jstk_clk <= '1';
                               outbit <= '0';
                when "0100" => jstk_clk <= '1';
                               outbit <= '0';
                when "0101" => jstk_clk <= '1';
                               outbit <= '0';
                when "0110" => jstk_clk <= '1';
                               tmpvalue(30) <= inbit;
                               outbit <= '0';
                when "0111" => jstk_clk <= '1';
                               tmpvalue(31) <= inbit;
                               outbit <= '0';
                               counter <= "0000";
                               current_sub_state <= second_wait;
                when others => counter <= "0000";
                               current_sub_state <= nothing;
              end case;
            elsif current_sub_state = second_wait then
              -- Vi v�ntar tio klockcykler mellan andra och tredje byten.
              counter <= counter + 1;
              outbit <= '0';
              if counter >= 10 then
                current_sub_state <= third_byte;
                counter <= "0000";
              end if;
            elsif current_sub_state = third_byte then
              -- �tta bitar till kommer. Vi vill ha allihopa igen.
              counter <= counter + 1;
              case counter is
                when "0000" => jstk_clk <= '1';
                               tmpvalue(12) <= inbit;
                               outbit <= '1';
                when "0001" => jstk_clk <= '1';
                               tmpvalue(13) <= inbit;
                               outbit <= '0';
                when "0010" => jstk_clk <= '1';
                               tmpvalue(14) <= inbit;
                               outbit <= '0';
                when "0011" => jstk_clk <= '1';
                               tmpvalue(15) <= inbit;
                               outbit <= '0';
                when "0100" => jstk_clk <= '1';
                               tmpvalue(16) <= inbit;
                               outbit <= '0';
                when "0101" => jstk_clk <= '1';
                               tmpvalue(17) <= inbit;
                               outbit <= '0';
                when "0110" => jstk_clk <= '1';
                               tmpvalue(18) <= inbit;
                               outbit <= '0';
                when "0111" => jstk_clk <= '1';
                               tmpvalue(19) <= inbit;
                               outbit <= '0';
                               counter <= "0000";
                               current_sub_state <= third_wait;
                when others => jstk_clk <= '1';
                               counter <= "0000";
                               current_meta_state <= waiting;
                               current_sub_state <= nothing;
              end case;
            elsif current_sub_state = third_wait then
              -- Vi v�ntar tio klockcykler mellan tredje och fj�rde byten.
              counter <= counter + 1;
              outbit <= '0';
              if counter >= 10 then
                current_sub_state <= fourth_byte;
                counter <= "0000";
              end if;
            elsif current_sub_state = fourth_byte then
              -- �tta bitar till kommer. �terigen vill vi ha de tv� f�rsta. Den
              -- h�r g�ngen l�ser vi d�remot de andra, oanv�nda, bitarna och
              -- l�gger dem i vektorn. Vi anv�nder dem inte, men om n�gon
              -- skulle vilja koppla in en mer avancerad joystick som skickar
              -- ut s�dana bitar till�ter detta det.
              counter <= counter + 1;
              case counter is
                when "0000" => jstk_clk <= '1';
                               outbit <= '1';
                               tmpvalue(0) <= inbit;
                when "0001" => jstk_clk <= '1';
                               outbit <= '0';
                               tmpvalue(1) <= inbit;
                when "0010" => jstk_clk <= '1';
                               outbit <= '0';
                               tmpvalue(2) <= inbit;
                when "0011" => jstk_clk <= '1';
                               outbit <= '0';
                               tmpvalue(3) <= inbit;
                when "0100" => jstk_clk <= '1';
                               outbit <= '0';
                               tmpvalue(4) <= inbit;
                when "0101" => jstk_clk <= '1';
                               outbit <= '0';
                               tmpvalue(5) <= inbit;
                when "0110" => jstk_clk <= '1';
                               -- Riktiga bitar! Tv� h�gsta bitarna i y-led.
                               tmpvalue(20) <= inbit;
                               outbit <= '0';
                when "0111" => jstk_clk <= '1';
                               tmpvalue(21) <= inbit;
                               outbit <= '0';
                               counter <= "0000";
                               current_sub_state <= fourth_wait;
                when others => counter <= "0000";
                               current_meta_state <= waiting;
                               current_sub_state <= nothing;
              end case;
            elsif current_sub_state = fourth_wait then
              -- Vi v�ntar tio klockcykler mellan fj�rde och femte byten.
              counter <= counter + 1;
              outbit <= '0';
              if counter >= 10 then
                current_sub_state <= fifth_byte;
                counter <= "0000";
              end if;
            elsif current_sub_state = fifth_byte then
              -- Av de sista �tta bitarna vill vi ha de sista tre som inneh�ller
              -- knapparnas status. Vi l�ser de andra ocks�, �ven om vi i v�rt
              -- program inte anv�nder dem, s� att n�gon annan skulle kunna
              -- koppla in en mer avancerad joystick om han vill.
              counter <= counter + 1;
              case counter is
                when "0000" => jstk_clk <= '1';
                               outbit <= '1';
                               tmpvalue(6) <= inbit;
                when "0001" => jstk_clk <= '1';
                               outbit <= '0';
                               tmpvalue(7) <= inbit;
                when "0010" => jstk_clk <= '1';
                               outbit <= '0';
                               tmpvalue(8) <= inbit;
                when "0011" => jstk_clk <= '1';
                               outbit <= '0';
                when "0100" => jstk_clk <= '1';
                               outbit <= '0';
                when "0101" => jstk_clk <= '1';
                               -- Riktiga bitar fr�n och med nu! Knappar.
                               tmpvalue(11) <= inbit;
                               outbit <='0';
                when "0110" => jstk_clk <= '1';
                               tmpvalue(10) <= inbit;
                               outbit <= '0';
                when "0111" => jstk_clk <= '1';
                               tmpvalue(9) <= inbit;
                               outbit <= '0';
                               outvalue <= tmpvalue;
                               write_enable_regs <= '1';
                when others => counter <= "0000";
                               write_enable_regs <= '0';
                               current_meta_state <= waiting;
                               current_sub_state <= nothing;
              end case;
            end if;
          else
            -- Om vi inte tar emot data f�r stunden h�js slave_select s� att inte
            -- styrspaken sitter och f�rv�ntar sig att vi skiftar in bitar.
            slave_select <= '1';
          end if;
        end if;
      end if;
    end if;
  end process;
end joystick_arc;

