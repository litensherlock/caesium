library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

--**********************************************************
-- send number of instructions, start address and then the instructions
-- send low end first. eg to send ABCD, you send first D then C then B then A
--*****************************************************
entity IO is
    Port ( clk,rst, rx : in  STD_LOGIC;
					new_out_data : in boolean;
					tx : out std_logic;
					our : out std_logic;
          seg: out  STD_LOGIC_VECTOR(7 downto 0);
          an : out  STD_LOGIC_VECTOR (3 downto 0);
					to_leds : in std_logic_vector( 15 downto 0);
					out_uart : in std_logic_vector (7 downto 0);
					address_out : out std_logic_vector (13 downto 0);
					instruction : out STD_LOGIC_VECTOR(31 downto 0);-- data out
					we_memory : out std_logic;
					CPU_reset : out std_logic;
					in_reg : out std_logic_vector (7 downto 0);
					load_puls_out : out std_logic);
end IO; 

architecture Behavioral of IO is
    component leddriver is
    Port ( clk : in  STD_LOGIC;
           seg : out  STD_LOGIC_VECTOR(7 downto 0);
           an : out  STD_LOGIC_VECTOR (3 downto 0);
           value : in  STD_LOGIC_VECTOR (15 downto 0));
    end component;

		type instruction_done_state is (IDLE , ONE , WAITING);
		type ready_state is (IDLE , READY , WAITING);

	--in
		signal load_puls : std_logic;
    signal sreg : STD_LOGIC_VECTOR(9 downto 0);  -- 10 bit skiftregister		     
    signal rx1,rx2 : std_logic;         -- vippor på insignalen
    signal sp : std_logic;              -- skiftpuls
    signal controller_count : integer := 0;
    signal load_count : integer := 0;
    signal shift_start : std_logic;
		
	--out		
		signal done_out : boolean;
		signal sp_out,shift_start_out : std_logic;
		signal out_data_buff : STD_LOGIC_VECTOR(8 downto 0);
		signal controller_count_out : integer := 0;
		signal load_count_out : integer := 0;

		signal memory_controller_start : std_logic;
		signal memory_controller_done : std_logic;
		signal all_done : std_logic; 
		signal value_buffer : std_logic_vector(15 downto 0);

-- MM demux
		signal MMpos : std_logic_vector (1 downto 0);
		signal reset_first : std_logic;

--number NS
		signal nsreg : std_logic_vector (7 downto 0);
		signal first_number : std_logic;
		signal number_ready : std_logic;
		signal number_state : ready_state;
		signal npos : std_logic;
		signal ns_done : std_logic;
		signal number : std_logic_vector (15 downto 0);
		signal number_count :std_logic_vector (15 downto 0);
		signal number_count_above_zero : std_logic;
		signal number_lp : std_logic;
		
		--IS
		signal ipos : STD_LOGIC_VECTOR(1 downto 0);
		signal isreg : std_logic_vector (7 downto 0);
		signal first_instruction : std_logic;
		signal instruction_ready : std_logic;
		signal instruction_lp : std_logic;
		signal is_done : std_logic;
		signal instruction_rdy_state : ready_state;

		signal is_state : instruction_done_state;
		 
		--AS
    signal address,address_buffer_out : std_logic_vector (13 downto 0);
		signal apos : STD_LOGIC;
		signal asreg : std_logic_vector (7 downto 0);
		signal address_ready : std_logic;
		signal address_rdy_state : ready_state;
		signal first_address : std_logic;
		signal address_count_enable : std_logic;
		signal address_lp : std_logic;
		signal as_done : std_logic;		
		
		
		constant controller_count_bit_value : integer :=868; -- delay betwen bits
		constant controller_count_startbit_value : integer :=controller_count_bit_value/2;
		constant load_count_reset_value : integer := 10;-- 10 bits in every transfer. 1 byte + start stop			
		constant start_bit : std_logic := '0';
		constant stop_bit : std_logic := '1';
		-- modes 

		constant memory_controller_sign : std_logic_vector(7 downto 0)	:= "00001111";
		constant start_cpu_sign : std_logic_vector(7 downto 0)					:= "01000010";
		constant stop_cpu_sign : std_logic_vector(7 downto 0)  					:= "10111101";
		

begin


-- port maps 

  -- *****************************
  -- * Multiplexad display       *
  -- *****************************
  led : leddriver port map (
			  clk => clk,
				seg => seg, 
				an => an, 
				value => value_buffer
				);
	value_buffer <= to_leds;--address_buffer_out;-- used to add the zeros to the vector used for leddriver.
	load_puls_out<=load_puls;
	our<=shift_start_out;
	
  -- *****************************
  -- *  synkroniseringsvippor    *
  -- *****************************
	process(clk) begin
		if rising_edge(clk) then
			if rst = '1' then
				rx1 <= '0';
				rx2 <= '0';
			else
				rx1 <= rx;
				rx2 <= rx1;
			end if;
		end if;
	end process;
	
	
		process(clk) begin
		if rising_edge(clk) then
			if new_out_data and shift_start_out='1' then -- we have new data and we are done with the old
				out_data_buff<=out_uart & start_bit;	
			elsif sp_out='1' then
				tx<=out_data_buff(0);
				out_data_buff(0)<=out_data_buff(1);
				out_data_buff(1)<=out_data_buff(2);
				out_data_buff(2)<=out_data_buff(3);
				out_data_buff(3)<=out_data_buff(4);
				out_data_buff(4)<=out_data_buff(5);
				out_data_buff(5)<=out_data_buff(6);
				out_data_buff(6)<=out_data_buff(7);
				out_data_buff(7)<=out_data_buff(8);
				out_data_buff(8)<=stop_bit;
			end if;
		end if;
	end process;

  -- *****************************
  -- *       master controller   * 
  -- *****************************
	
	-- shiftpulse and shift_start is done here
	-- this process is just shifting stuff in to sreg
  	process(clk) begin
		if rising_edge(clk) then
			if rst = '1' then
				sp_out <= '0';
				shift_start_out <= '1';
				
			-- if shift_start we check for the signal and nothing else.
			elsif shift_start_out = '1' then
				if new_out_data then					
					controller_count_out <= controller_count_startbit_value;
					sp_out <= '0';
					shift_start_out<='0';
				end if;
				
			elsif controller_count_out = 0 then
				sp_out <= '1';
				controller_count_out <= controller_count_bit_value;

				if done_out then
					shift_start_out<='1';
				end if;

			else controller_count_out <= controller_count_out - 1;
				sp_out <= '0';
				if done_out then
					shift_start_out<='1';
				end if;
			end if;
		end if;
	end process;
	
	-- done_out is done here
  	process(clk) begin	
		if rising_edge(clk) then
			if rst = '1' then
				load_count_out<=load_count_reset_value;
				done_out<=false;

			elsif load_count_out = 0  then
				load_count_out <= load_count_reset_value;			
				done_out <= true;
				
			elsif sp_out='1' then
				load_count_out <= load_count_out - 1;
				done_out <=false;
			else 
				done_out <=false;
			end if;
		end if;
	end process;
	
-- shiftpulse and shift_start is done here
-- this process is just shifting stuff in to sreg
  	process(clk) begin
		if rising_edge(clk) then
			if rst = '1' then
				sp <= '0';
				shift_start <= '1';
				
			-- if shift_start we check for the start bit and nothing else.
			elsif shift_start = '1' then
				if rx1 = '0' and rx2 = '1' then
					shift_start <= '0';
					controller_count <= controller_count_startbit_value;
					sp <= '0';
				end if;
				
			elsif controller_count = 0 then
				sp <= '1';
				controller_count <= controller_count_bit_value;

				if load_puls='1' then
					shift_start<='1';
				end if;

			else controller_count <= controller_count - 1;
				sp <= '0';
				if load_puls='1' then
					shift_start<='1';
				end if;
			end if;
		end if;
	end process;

-- loadpulse is done here
  	process(clk) begin	
		if rising_edge(clk) then
			if rst = '1' then
				load_count<=load_count_reset_value;
				load_puls<='0';

			elsif load_count = 0  then
				load_count <= load_count_reset_value;			
				load_puls <= (not sreg(0)) and sreg(9);
				
			elsif sp='1' then
				load_count <= load_count - 1;
				load_puls <='0';
			else 
				load_puls <='0';
			end if;
		end if;
	end process;


  -- *****************************
  -- *       mode controller     * 
  -- *****************************

--check what mode to go to.
--if it gets a bitpattern that matches a mode and its not in a mode atm then send start signal to that mode.
	all_done <= memory_controller_done;
	process(clk) begin
		if rising_edge(clk) then
			if rst = '1' then
				cpu_reset <= '1';
				memory_controller_start <= '0';
			elsif (sreg(8 downto 1) =  memory_controller_sign) and load_puls = '1' and all_done='1' then
				memory_controller_start <='1';
				cpu_reset<='1';
			elsif (sreg(8 downto 1) =  start_cpu_sign) and load_puls = '1' and all_done='1' then	
				cpu_reset<='0';
				memory_controller_start <='0';
			elsif (sreg(8 downto 1) =  stop_cpu_sign) and load_puls = '1' and all_done='1' then	
				cpu_reset<='1';
				memory_controller_start <='0';
-- memory start is used so we dont think we are done before the memory controller gets the time to set its done signal to 0
			elsif all_done='1' and memory_controller_start='0' then 
				memory_controller_start <='0';
			else
 				memory_controller_start <='0';
			end if;
		end if;
	end process;
  
  -- *****************************
  -- *     memory  controller    * 
  -- *****************************

-- first you get memory_controller_start that pushes MMpos to  mode, then as get done and pushes Mmpos
-- to number of instructions and so on.

  	process(clk) begin
		if rising_edge(clk) then
			if rst = '1' then			
				MMpos <= "00"; -- 3
				memory_controller_done <= '1';
			elsif memory_controller_start='1' or ns_done='1' or as_done='1' or is_done='1' then
				MMpos <= MMpos + 1;
				memory_controller_done <= '0';
			elsif MMpos = "00" then
				memory_controller_done <='1';
			end if;
		end if;
	end process;

we_memory<=instruction_ready;

  -- *****************************
  -- * 10 bit skiftregister      *
  -- *****************************
	process(clk) begin
		if rising_edge(clk) then
			if rst = '1' then
				sreg <= "0000000000";
			elsif sp = '1' then
				sreg(9)<= rx2;
				sreg(8)<= sreg(9);
				sreg(7)<= sreg(8);
				sreg(6)<= sreg(7);
				sreg(5)<= sreg(6);
				sreg(4)<= sreg(5);
				sreg(3)<= sreg(4);
				sreg(2)<= sreg(3);
				sreg(1)<= sreg(2);
				sreg(0)<= sreg(1);		
			end if;
		end if;
	end process;
	

  -- *****************************
  -- * Load_puls demux           *
  -- *****************************

  process(clk) begin
		if rising_edge(clk) then
			if rst = '1' then			
				instruction_lp <='0';
				address_lp<='0';
				number_lp<='0';
			elsif MMpos = 1 then
				number_lp<='0';
				address_lp<=load_puls;
				instruction_lp <='0';
			elsif MMpos = 2 then				
				number_lp<=load_puls;
				address_lp <= '0';				
				instruction_lp<= '0';
			elsif MMpos = 3 then
				number_lp<='0';
				address_lp<='0';
				instruction_lp <=load_puls;
			else
				address_lp<='0';
				number_lp<='0';
				instruction_lp <='0';
			end if;
		end if;
	end process;	

	-- move the in data to the readable register
	process(clk) begin
		if rising_edge(clk) then
			if rst = '1' then
				in_reg <= "00000000";
			elsif load_puls = '1' then
				in_reg<=sreg (8 downto 1);					
			end if;
		end if;
	end process;
  -- *****************************
  -- * sreg demux   MM           *
  -- *****************************
	process(clk) begin
    		if rising_edge(clk) then
			if rst = '1' then
				isreg <= "00000000";
				asreg <= "00000000";
				nsreg <= "00000000";
				reset_first <= '0';
			elsif MMpos = 1 then
				isreg <= "00000000";
				asreg <= sreg(8 downto 1); -- address
				nsreg <= "00000000";
				reset_first <= '0';		
			elsif MMpos = 2 then
				isreg <= "00000000";
				asreg <= "00000000";
				nsreg <= sreg(8 downto 1); -- number
				reset_first <= '0';	
			elsif MMpos = 3 then
				isreg <= sreg(8 downto 1); -- instruction
				asreg <= "00000000";
				nsreg <= "00000000";
				reset_first <= '0';
			elsif MMpos= 0 then
				reset_first <= '1';

			end if;
		end if;
	end process;

  -- *****************************
  -- * 2  bit räknare   IS       *
  -- *****************************

  	process(clk) begin
		if rising_edge(clk) then
			if rst = '1' then
				ipos <= "00";
				first_instruction <='1';
			elsif instruction_lp = '1' then
				ipos <= ipos + 1;
				if ipos = "00" then -- done a full lap.					
					first_instruction <='0';			
				end if;
			elsif reset_first='1' then
					first_instruction <= '1';
			end if;
		end if;
	end process;

	process(clk) begin
		if rising_edge(clk) then
			if rst = '1' then
				instruction_rdy_state <= IDLE;
				instruction_ready <= '0';
		elsif instruction_rdy_state=IDLE and first_instruction = '0' and ipos = "00" then
				instruction_rdy_state <= READY;
				instruction_ready <= '1';
			elsif instruction_rdy_state = READY then
				instruction_rdy_state <= WAITING;
				instruction_Ready <='0';
			elsif instruction_rdy_state = WAITING and instruction_lp = '1' then
				instruction_rdy_state <= IDLE;
				instruction_Ready <='0';
			end if;
		end if;
	end process;


  -- *********************************
  -- * 32 bit inst register med demux*
  -- *********************************

	process(clk) begin
		if rising_edge(clk) then
			if rst = '1' then
				instruction <= "00000000000000000000000000000000";
			elsif instruction_lp = '1' then
				if ipos = 3 then
					instruction(7 downto 0) <= isreg (7 downto 0);
				elsif ipos =2 then
					instruction(15 downto 8) <= isreg (7 downto 0);
				elsif ipos =1 then
					instruction(23 downto 16) <= isreg (7 downto 0);
				elsif ipos=0 then
					instruction(31 downto 24) <= isreg (7 downto 0);
				end if;
			end if;
		end if;
	end process;

  -- ***************************** 
  -- * 1  bit räknare   AS       *
  -- *****************************

  	process(clk) begin
		if rising_edge(clk) then
			if rst = '1' then
				apos <= '0';
			-- when we get the load puls we do stuff
			elsif address_lp = '1' then
				-- räkna /ta komplement  apos <= apos +1;
				if apos ='0' then
					apos<= '1';
				else 
					apos <= '0';
				end if;

				if apos = '0' then -- done a full lap so we are done.
					first_address <= '0';		
				end if;	

			elsif reset_first= '1' then -- can not enter here if we got a ready address anyway sens MMpos is 3
					first_address <= '1';
			end if;
		end if;
	end process;

	process(clk) begin
		if rising_edge(clk) then
			if rst = '1' then
				address_rdy_state <= IDLE;
				address_ready <= '0';
		elsif address_rdy_state=IDLE and first_address = '0' and apos = '0' then
				address_rdy_state <= READY;
				address_ready <= '1';
			elsif address_rdy_state = READY then
				address_rdy_state <= WAITING;
				address_Ready <='0';
			elsif address_rdy_state = WAITING and address_lp = '1' then
				address_rdy_state <= IDLE;
				address_Ready <='0';
			end if;
		end if;
	end process;

  -- ***************************************
  -- * 14 bit address register med demux AS *
  -- ***************************************

	process(clk) begin
		if rising_edge(clk) then
			if rst = '1' then
				address <= "00000000000000";
				as_done<= '0';
			elsif address_lp = '1' then
				if apos='0' then
					address(13 downto 8) <= asreg (5 downto 0);
				elsif apos='1' then
					address(7 downto 0) <= asreg (7 downto 0);
					as_done <= '1';
				end if;
			else 
				as_done <='0';
			end if;
		end if;
	end process;

  -- ***************************************
  -- * address counter 										 *
  -- ***************************************

	process(clk) begin
		if rising_edge(clk) then
			if rst = '1' then
				address_buffer_out <="00000000000000";
			elsif address_ready='1' then
				address_buffer_out <=address;
			elsif address_count_enable='1' then
				address_buffer_out<=address_buffer_out + 1;
			end if;
		end if;
	end process;
	address_out <= address_buffer_out; -- cant do aritmatic on out signal so we use the buffer insted.

  -- ***************************** 
  -- * 1  bit räknare   NS       *
  -- *****************************

  process(clk) begin
		if rising_edge(clk) then
			if rst = '1' then
				npos <= '0';		
			elsif number_lp = '1' then

				-- räkna /ta komplement  npos <= npos +1;
				if npos ='0' then
					npos<= '1';
				else 
					npos <= '0';
				end if;
				
				if npos = '0' then -- done a full lap so we are done.
					first_number<='0';				
				end if;
			
			elsif reset_first='1' then -- should not be able to enter here if we got a ready number sens MMpos will be at 3
				first_number <= '1';				
			end if;
		end if;
	end process;

--one puls to number_Ready, reset when this memory write is done.
	process(clk) begin
		if rising_edge(clk) then
			if rst = '1' then
				number_state <= IDLE;
				number_ready <= '0';
			elsif number_state=IDLE and first_number = '0' and npos = '0' then
				number_state <= READY;
				number_ready <= '1';
			elsif number_state = READY then
				number_State <= WAITING;
				number_Ready <='0';
			elsif number_state = WAITING and reset_first = '1' then
				number_state <= IDLE;
				number_Ready <='0';
			end if;
		end if;
	end process;

  -- ***************************************
  -- * 16 bit number register with demux NS*
  -- ***************************************

	process(clk) begin
		if rising_edge(clk) then
			if rst = '1' then
				number <= "0000000000000000";
			elsif number_lp = '1' then
				if npos = '0' then
					number(15 downto 8) <= nsreg;					
				elsif npos = '1' then
					number(7 downto 0) <= nsreg;
				end if;
			end if;
		end if;
	end process;

  -- ***************************************
  -- * number of addresses counter 				 *
  -- ***************************************

	process(clk) begin
		if rising_edge(clk) then
			if rst = '1' then
				number_count<="0000000000000000";
				ns_done <='0';
			elsif number_ready='1' then
				number_count <= number;
				ns_done <='1';
			elsif instruction_ready='1' then
				number_count <= number_count - 1;
				ns_done <='0';
			else 
				ns_done <='0';
			end if;
		end if;
	end process;		

	number_count_above_zero <= '0' WHEN number_count = "0000000000000000" ELSE '1';

	address_count_enable <= number_count_above_zero and instruction_ready;

	process(clk) begin
		if rising_edge(clk) then
			if is_state=IDLE and number_count_above_zero = '0' then
				is_state <= ONE;
				is_done <= '1';
			elsif is_state=ONE then
				is_state <= WAITING;
				is_done <= '0';
			elsif is_state = WAITING and number_count_above_zero ='1' then
				is_state <= IDLE;
			end if;
		end if;
	end process;

end Behavioral;

