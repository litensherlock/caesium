
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity steg3 is
  port(clk : in std_logic;
       OP_in : in std_logic_vector(5 downto 0);
			 OP_ir4_in : in std_logic_vector(5 downto 0);
			 dreg_in : in std_logic_vector(4 downto 0);
			 breg_in : in std_logic_vector(4 downto 0);
			 dreg_steg4_in : in std_logic_vector(4 downto 0);
       data_from_steg2_alu : in std_logic_vector(31 downto 0);
			 constant_in : in std_logic_vector(15 downto 0);			 
			 dataforwarded_address_steg4 : in std_logic_vector(15 downto 0);			 
       
       current_OP : out std_logic_vector(5 downto 0);
			 mode_out : out std_logic_vector (1 downto 0);	
			 write_enable_data_memory: out STD_LOGIC;
			 data_to_data_memory : out std_logic_vector(31 downto 0);
       address_to_data_memory : out std_logic_vector(15 downto 0);
			 dreg_out : out std_logic_vector(4 downto 0);
       data_to_steg4 : out std_logic_vector(31 downto 0);
       dataforward_steg3 : out std_logic_vector(31 downto 0)
       );
end steg3;

architecture steg3_arc of steg3 is

  signal OP_ir3,OP_ir4 : std_logic_vector(5 downto 0);
	signal dreg_steg4,breg : std_logic_vector(4 downto 0);	
  signal current_d3 : std_logic_vector(31 downto 0);
	signal current_const : std_logic_vector(15 downto 0);
	signal chosen_address : std_logic_vector(15 downto 0);
	signal steg4_writes_to_register : boolean;
	
	
-- constants
	constant ADD_r :std_logic_vector (5 downto 0) := "000001";
	constant ADD_c :std_logic_vector (5 downto 0) := "000010";
	constant SUB_r :std_logic_vector (5 downto 0) := "000011";
	constant SUB_c :std_logic_vector (5 downto 0) := "000100";
	constant LOAD :std_logic_vector (5 downto 0) := "000101";
	constant STORE :std_logic_vector (5 downto 0) := "000110";
	constant CMP_r :std_logic_vector (5 downto 0) := "000111";
	constant JMP :std_logic_vector (5 downto 0) := "001000";
	constant JOour :std_logic_vector (5 downto 0) := "010001";
	constant JOZ :std_logic_vector (5 downto 0) := "001010";
	constant JNZ :std_logic_vector (5 downto 0) := "001011";
	constant JOC :std_logic_vector (5 downto 0) := "001100";
	constant JNC :std_logic_vector (5 downto 0) := "001101";
	constant JON :std_logic_vector (5 downto 0) := "001110";
	constant JNN :std_logic_vector (5 downto 0) := "001111";
	constant JOA :std_logic_vector (5 downto 0) := "010000";
	constant JNA :std_logic_vector (5 downto 0) := "010001";	
	constant JOV :std_logic_vector (5 downto 0) := "010010";
	constant JNV :std_logic_vector (5 downto 0) := "010011";
	constant JOiur :std_logic_vector (5 downto 0) := "010100";
	constant JNiur :std_logic_vector (5 downto 0) := "010101";		
	
	constant JNour :std_logic_vector (5 downto 0) := "011000";
	constant LSR :std_logic_vector (5 downto 0) := "011001";
	constant ASR :std_logic_vector (5 downto 0) := "011010";
	constant XSL :std_logic_vector (5 downto 0) := "011011";
	constant CNTRL :std_logic_vector (5 downto 0) := "011100";
	constant AND_r :std_logic_vector (5 downto 0) := "011101";
	constant OR_r :std_logic_vector (5 downto 0) := "011110";
	constant add_c_no_flags :std_logic_vector (5 downto 0) := "011111";
	constant push_pc :std_logic_vector (5 downto 0) := "100000";
	
	constant pop_pc :std_logic_vector (5 downto 0) := "100010";
	constant xor_r :std_logic_vector (5 downto 0) := "100011";
	constant sub_c_no_flags :std_logic_vector (5 downto 0) := "100100";
	constant CMP_c :std_logic_vector (5 downto 0) := "100101";
	constant CLEAR :std_logic_vector (5 downto 0) := "100110";
	constant MOVE :std_logic_vector (5 downto 0) := "100111";
	constant PUT :std_logic_vector (5 downto 0) := "101000";
	constant GET :std_logic_vector (5 downto 0) := "101001";
	constant put_b :std_logic_vector (5 downto 0) := "101010";
	constant get_b :std_logic_vector (5 downto 0) := "101011";
	constant store_b :std_logic_vector (5 downto 0) := "101100";
	constant load_b :std_logic_vector (5 downto 0) := "101101";
	constant put_w :std_logic_vector (5 downto 0) := "101110";
	constant get_w :std_logic_vector (5 downto 0) := "101111";
	constant store_w :std_logic_vector (5 downto 0) := "110000";
	constant load_w :std_logic_vector (5 downto 0) := "110001";	
	constant JMP_r :std_logic_vector (5 downto 0) := "110010";
	constant joz_r :std_logic_vector (5 downto 0) := "110011";
	constant Jnz_r :std_logic_vector (5 downto 0) := "110100";
	constant Joc_r :std_logic_vector (5 downto 0) := "110101";
	constant Jnc_r :std_logic_vector (5 downto 0) := "110110";
	constant Jon_r :std_logic_vector (5 downto 0) := "110111";
	constant Jnn_r :std_logic_vector (5 downto 0) := "111000";
	constant Jov_r :std_logic_vector (5 downto 0) := "111001";
	constant Jnv_r :std_logic_vector (5 downto 0) := "111011";
	constant Joiur_r :std_logic_vector (5 downto 0) := "111100";
	constant Jniur_r :std_logic_vector (5 downto 0) := "111101";
	constant Joour_r :std_logic_vector (5 downto 0) := "111110";
	constant Jnour_r :std_logic_vector (5 downto 0) := "111111";
	
	--more constants
	constant long : std_logic_vector (1 downto 0) := "00";
	constant word : std_logic_vector (1 downto 0) := "01";
	constant byte : std_logic_vector (1 downto 0) := "10";
	

begin
  data_to_steg4 <= current_d3;
  current_OP <= OP_ir3;
	
	--********************************
	--**      memory area           **
	--********************************
	
	--adress till dataminnet väljs.
  with OP_ir3 select
    address_to_data_memory <= chosen_address when push_pc|pop_pc|PUT|GET|put_b|get_b|put_w|get_w,
    current_const when others;

  
	-- Write enable till dataminne
  with OP_ir3 select
    write_enable_data_memory <= '1' when STORE|store_b|store_w|push_pc|PUT|put_b|put_w,
    '0' when others;
  
 
	--mode to memory. byte/long
	with OP_ir3 select
    mode_out <= byte when get_b | put_b | load_b | store_b,
								word when get_w | put_w | load_w | store_w,
								long when others;
	

-- Om steg x skriver till ett register och steg x skriver till samma register
-- som jag ska hämta värde ifrån.
  chosen_address <= dataforwarded_address_steg4 when steg4_writes_to_register and 
										dreg_steg4 = breg else
										current_d3(15 downto 0);
	
	with OP_ir4 select
    steg4_writes_to_register <= true when ADD_r|ADD_c|SUB_r|sub_c|LOAD|load_b|load_w|LSR|ASR|XSL|AND_r|OR_r|xor_r|CLEAR|MOVE|GET|get_b|add_c_no_flags|sub_c_no_flags,
    false when others;	
  
-- Klockat register för nuvarande instruktion, samt det data som kommer från alun i steg2
  process(clk)
  begin
    if rising_edge(clk) then
      OP_ir3 <= OP_in;
			OP_ir4 <= OP_ir4_in;
			current_const<=constant_in;
			breg<=breg_in;
			dreg_out<=dreg_in;
			dreg_steg4<=dreg_steg4_in;
      current_d3 <= data_from_steg2_alu;
    end if;
  end process;
	data_to_data_memory<= current_d3;
  dataforward_steg3 <= current_d3; 
end steg3_arc;