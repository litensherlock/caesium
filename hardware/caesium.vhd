library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.STD_LOGIC_UNSIGNED.ALL;



entity caesium is
  Port (clk, rx, rst : in STD_LOGIC;
	tx : out std_logic;
	--switch: in STD_LOGIC_VECTOR(7 downto 0);
	vga, seg: out STD_LOGIC_VECTOR(7 downto 0);
	an: out STD_LOGIC_VECTOR(3 downto 0);
	--hsync, vsync, jstk_MOSI, jstk_MISO, jstk_CLK, jstk_SS: out STD_LOGIC;
	Led : out std_logic_vector (7 downto 0));
end caesium; 

architecture caesium_arc of caesium is

	component IO is
			Port ( clk,rst, rx : in  STD_LOGIC;
					new_out_data : in boolean;
					tx : out std_logic;
					our : out std_logic;
          seg: out  STD_LOGIC_VECTOR(7 downto 0);
          an : out  STD_LOGIC_VECTOR (3 downto 0);
					to_leds : in std_logic_vector( 15 downto 0);
					out_uart : in std_logic_vector (7 downto 0);
					address_out : out std_logic_vector (13 downto 0);
					instruction : out STD_LOGIC_VECTOR(31 downto 0);-- data out
					we_memory : out std_logic;
					CPU_reset : out std_logic;
					in_reg : out std_logic_vector (7 downto 0);
					load_puls_out : out std_logic);
	end component;

  component CPU is
    port(clk : in std_logic;				 
				 iur : in std_logic;
         reset : in std_logic;		
				 our : in std_logic;
         instruction_from_instruction_memory : in std_logic_vector(31 downto 0);
         data_from_regs1 : in std_logic_vector(31 downto 0);
         data_from_regs2 : in std_logic_vector(31 downto 0);
         data_from_data_memory : in std_logic_vector(31 downto 0);
				 reg0_in : in std_logic_vector(13 downto 0);
         address_to_instruction_memory : out std_logic_vector(13 downto 0);
         address_to_regs1 : out std_logic_vector(4 downto 0);
         address_to_regs2 : out std_logic_vector(4 downto 0);
         address_to_regs3 : out std_logic_vector(4 downto 0);
         address_to_data_memory : out std_logic_vector(15 downto 0);
         data_to_regs : out std_logic_vector(31 downto 0);
         data_to_data_memory : out std_logic_vector(31 downto 0);
         write_enable_to_regs : out std_logic;
         write_enable_to_data_memory : out std_logic;
         controll_joystick : out std_logic;
				 mode_out : out std_logic_vector (1 downto 0)	
         );
  end component;

	component data_ram is
		port (clk,we,we2 : in STD_LOGIC;
					new_out_data : out boolean;
					in_uart_ready : in std_logic;					
					mode2 : in std_logic_vector (1 downto 0);
					out_data, out_data2: out STD_LOGIC_VECTOR(31 downto 0);
					in_data, in_data2: in STD_LOGIC_VECTOR(31 downto 0);					
					in_uart : in std_logic_vector (7 downto 0);
					in_address : in std_logic_vector ( 13 downto 0);
					in_address2 : in std_logic_vector(15 downto 0);
					to_uart : out std_logic_vector (7 downto 0);
					iur : out std_logic);
	end component;

  component REGS is
    port(clk, we, we_jstk, stall_from_cpu : in std_logic;
         rst: in STD_LOGIC;
         data3_in, data_jstk_in: in STD_LOGIC_VECTOR(31 downto 0);
         addr1, addr2, addr3: in STD_LOGIC_VECTOR(4 downto 0);
         
         data1_out, data2_out: out STD_LOGIC_VECTOR(31 downto 0); 
				 reg0_out : out std_logic_vector(13 downto 0);				 

         pal0, pal1, pal2, pal3: out std_logic_vector(7 downto 0));
  end component;

  signal IO_address, GU_address,inst_address_from_cpu,write_address_from_cpu,
					read_address_from_cpu: std_logic_vector (13 downto 0);
	
	signal data_address_from_cpu : std_logic_vector (15 downto 0);
					
  signal IO_data, GU_data, regs_data_out_from_cpu,other_in_data: STD_LOGIC_VECTOR(31 downto 0);
	
  signal cpu_instruction, cpu_data_in, data_to_cpu, data_from_cpu, data_from_reg_1,
					data_from_reg_2: std_logic_vector (31 downto 0);
					
  signal we_memory, cpu_reset, GU_using_memory,
				we_from_cpu,write_enable_regs_from_cpu,joystick_read,jstk_write_enable_regs,
				stall_register: std_logic;

  signal pal0, pal1, pal2, pal3, pal4, pal5, pal6, pal7,
				pal8, pal9, pal10, pal11, pal12, pal13, pal14, pal15,out_uart: STD_LOGIC_VECTOR(7 downto 0);

  signal reg_addr_1_from_cpu, reg_addr_2_from_cpu, reg_addr_3_from_cpu: STD_LOGIC_VECTOR(4 downto 0);
	signal data_mode : std_logic_vector (1 downto 0);
  -- interna signaler
	signal to_leds: std_logic_vector (15 downto 0);
	signal other_address,reg0 : std_logic_vector (13 downto 0);

  signal cpu_reset_signal, buffwe, new_data_in_uart, iur,our : std_logic;
	signal new_out_data : boolean;
	signal in_uart : std_logic_vector (7 downto 0);
begin

	other_address <= IO_address when we_memory = '1' else
									inst_address_from_cpu;
	
  cpu_reset_signal <= rst or cpu_reset;   
  
	to_leds<=pal2 & pal3;
	
  Led <= pal1;

  IO_comp: IO port map (
    --in
    clk=>clk,
    rst=>rst,
    rx=>rx,
    to_leds => to_leds,
		new_out_data=>new_out_data,
		out_uart=>out_uart,
		
    --out
		tx=>tx,
		our=>our,
    seg=>seg,
    an=>an,
		address_out=>IO_address,
		instruction=>IO_data,
		cpu_reset=>cpu_reset,
		we_memory=>we_memory,
		in_reg=>in_uart,
		load_puls_out=>new_data_in_uart
    );

 
  REGS_comp: regs port map (
    -- in
    clk => clk,
    rst => rst,
    we => write_enable_regs_from_cpu, 
    we_jstk => '0',
    data3_in => regs_data_out_from_cpu,
    data_jstk_in => x"00000000",
    addr1 => reg_addr_1_from_cpu,
    addr2 => reg_addr_2_from_cpu, 
    addr3 => reg_addr_3_from_cpu,
    stall_from_cpu => '0',
-- ut      
    data1_out => data_from_reg_1,
    data2_out => data_from_reg_2,   
		reg0_out=> reg0,
    pal0 => pal0, 
    pal1 => pal1,
    pal2 => pal2, 
    pal3 => pal3
    );

		
  cpu_comp: cpu port map(
		--in
		iur=>iur,
    clk => clk,
		our=>our,
    reset => cpu_reset_signal,
    instruction_from_instruction_memory => cpu_instruction,
    data_from_regs1 => data_from_reg_1,
    data_from_regs2 => data_from_reg_2,
    data_from_data_memory => data_to_cpu,
		reg0_in=>reg0,
		
		--out
		data_to_data_memory => data_from_cpu,
		controll_joystick => joystick_read,
		write_enable_to_data_memory => we_from_cpu,
		write_enable_to_regs => write_enable_regs_from_cpu,
		data_to_regs => regs_data_out_from_cpu,
		address_to_data_memory => data_address_from_cpu,
		address_to_regs1 => reg_addr_1_from_cpu,
    address_to_regs2 => reg_addr_2_from_cpu,
    address_to_regs3 => reg_addr_3_from_cpu,
		address_to_instruction_memory => inst_address_from_cpu,
		mode_out => data_mode
    );

		
	data_ram_comp: data_ram port map (
		--in
		clk=> clk,
		we=>we_memory,
		we2 =>we_from_cpu,
		mode2=> data_mode,
		in_data=> IO_data,
		in_data2=>data_from_cpu,
		in_address=>other_address,
		in_address2=>data_address_from_cpu,
		in_uart_ready=>new_data_in_uart,
		in_uart=>in_uart,
		
		--out
		out_data=>cpu_instruction,
		out_data2=>data_to_cpu,
		to_uart=>out_uart,
		new_out_data=>new_out_data,
		iur=>iur
		
		);
		  
end caesium_arc;
