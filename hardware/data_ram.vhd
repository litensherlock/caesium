-------------------------------------------------------------------------------
-- Sammanslaget RAM:
-- Definierar RAM som fyra klumpar ram á 16384 adresser
-- där man läser alla fyra vid long, två vid word och en vid byte
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity data_ram is
  port (clk,we,we2 : in STD_LOGIC;
				in_uart_ready : in std_logic;
				mode2 : in std_logic_vector (1 downto 0);        
				in_data, in_data2: in STD_LOGIC_VECTOR(31 downto 0);
				in_uart : in std_logic_vector (7 downto 0);
				in_address : in std_logic_vector(13 downto 0);
				in_address2 : in std_logic_vector(15 downto 0);
				iur : out std_logic;
				new_out_data : out boolean;
				to_uart : out std_logic_vector (7 downto 0);
				out_data, out_data2: out STD_LOGIC_VECTOR(31 downto 0));
end data_ram;

architecture data_ram_arc of data_ram is
	component ram_block is
	port (clk,we,we2 : in STD_LOGIC;
        out_data, out_data2: out STD_LOGIC_VECTOR(7 downto 0);
				in_data, in_data2: in STD_LOGIC_VECTOR(7 downto 0);
				in_address,in_address2 : in std_logic_vector(13 downto 0));
	end component;
  
	signal long_address, long_address2: std_logic_vector(13 downto 0);
	signal shadow_address,shadow_address2: std_logic_vector(1 downto 0);
	
	signal shadow_mode,shadow_mode2 : std_logic_vector(1 downto 0);
	signal shadow_uart : std_logic_vector (7 downto 0);
	
	signal in_data_byte0,in_data_byte1,in_data_byte2,in_data_byte3 : std_logic_vector (7 downto 0);
	signal in_data2_byte0,in_data2_byte1,in_data2_byte2,in_data2_byte3 : std_logic_vector (7 downto 0);
	
	signal out_data_byte0,out_data_byte1,out_data_byte2,out_data_byte3 : std_logic_vector (7 downto 0);
	signal out_data2_byte0,out_data2_byte1,out_data2_byte2,out_data2_byte3 : std_logic_vector (7 downto 0);
	
	signal out_byte : std_logic_vector (7 downto 0);
	signal out_word : std_logic_vector (15 downto 0);
	signal out_byte2 : std_logic_vector (7 downto 0);
	signal out_word2 : std_logic_vector (15 downto 0);
	
  signal we_byte0,we_byte1,we_byte2,we_byte3 : std_logic;
	signal we2_byte0,we2_byte1,we2_byte2,we2_byte3 : std_logic;	
	
	constant long : std_logic_vector (1 downto 0) := "00";
	constant word : std_logic_vector (1 downto 0) := "01";
	constant byte : std_logic_vector (1 downto 0) := "10";
	constant uart : std_logic_vector (1 downto 0) := "11";
	
	constant zero_word : std_logic_vector (15 downto 0) := "0000000000000000";
	constant zero_byte : std_logic_vector (23 downto 0) := "000000000000000000000000";
	constant uart_address : std_logic_vector (15 downto 0) := "0000000000000000";
	
begin

	long_address <=in_address;
	long_address2 <=in_address2(15 downto 2);
	
	--check if we write to uart
	process(clk)
	begin
		if rising_edge(clk) then
			if in_address2=uart_address and mode2=byte and we2='1' then
				to_uart<=in_data2 (7 downto 0);
				new_out_data<=true;
			else
				new_out_data<=false;
			end if;
		end if;
	end process;
		
	--make write enable signals
	we_byte3 <= '1' when we = '1' else
							'0';
							
	we_byte2 <= '1' when we = '1' else
							'0';
							
	we_byte1 <= '1' when we = '1' else
							'0';
							
	we_byte0 <= '1' when we = '1' else							
							'0';
							
	--make write enable signals
	we2_byte3 <= '1' when we2 = '1'and mode2 = long else
							'1' when we2 = '1'and mode2 = word and in_address2 (1 downto 0) = "10" else
							'1' when we2 = '1'and mode2 = byte and in_address2 (1 downto 0) = "11" else
							'0';
	we2_byte2 <= '1' when we2 = '1'and mode2 = long else
							'1' when we2 = '1'and mode2 = word and in_address2 (1 downto 0) = "10" else
							'1' when we2 = '1'and mode2 = byte and in_address2 (1 downto 0) = "10" else
							'0';
	we2_byte1 <= '1' when we2 = '1'and mode2 = long else
							'1' when we2 = '1'and mode2 = word and in_address2 (1 downto 0) = "00" else
							'1' when we2 = '1'and mode2 = byte and in_address2 (1 downto 0) = "01" else
							'0';
	we2_byte0 <= '1' when we2 = '1'and mode2 = long else
							'1' when we2 = '1'and mode2 = word and in_address2 (1 downto 0) = "00" else
							'1' when we2 = '1'and mode2 = byte and in_address2 (1 downto 0) = "00" and in_address2/=uart_address else
							'0';
							
							
	
	-- make in data bytes
	in_data_byte3 <= in_data (31 downto 24);
									 
	in_data_byte2 <= in_data (23 downto 16);									
									 
	in_data_byte1 <= in_data (15 downto 8);
									 
	in_data_byte0 <= in_data (7 downto 0);
	
	
		-- make in data bytes
	in_data2_byte3 <= in_data2 (31 downto 24) when mode2 = long else
									 in_data2 (15 downto 8) when mode2 = word else
									 in_data2 (7 downto 0);
									 
	in_data2_byte2 <= in_data2 (23 downto 16) when mode2 = long else
									 in_data2 (7 downto 0);-- lower part of word and byte is always the lowest byte of in data
									 
	in_data2_byte1 <= in_data2 (15 downto 8) when mode2 = long else
									 in_data2 (15 downto 8) when mode2 = word else
									 in_data2 (7 downto 0);
									 
	in_data2_byte0 <= in_data2 (7 downto 0);
		
	--save relevant address bits (the low bits)
	process(clk)
	begin
		if rising_edge(clk) then
			shadow_address2 <= in_address2(1 downto 0);
		end if;
	end process;
	
	--save the mode for the out data
		
	process(clk)
	begin
		if rising_edge(clk) then
			if mode2=byte and long_address2=uart_address then
				shadow_mode2<=uart;
			else
				shadow_mode2 <= mode2;
			end if;	
		end if;
	end process;
	
	--set / reset the unread data flag
	process(clk)
	begin
	 if rising_edge(clk) then
			if in_uart_ready='1' then
				iur<='1';
			elsif mode2=uart then
				iur<='0';
			end if;
		end if;
	end process;
		
	--just to make sure we are reading whats in the uart at the time we get the request
	process(clk)
	begin
		if rising_edge (clk) then
			shadow_uart<=in_uart;
		end if;
	end process;
	
	block0: ram_block port map (
		clk=>clk,
		we=>we_byte0,
		we2=>we2_byte0,
		in_data=>in_data_byte0,
		in_data2=>in_data2_byte0,
		in_address=>long_address,
		in_address2=>long_address2,
		out_data=>out_data_byte0,
		out_data2=>out_data2_byte0		
		);
	
	block1: ram_block port map (
		clk=>clk,
		we=>we_byte1,
		we2=>we2_byte1,
		in_data=>in_data_byte1,
		in_data2=>in_data2_byte1,
		in_address=>long_address,
		in_address2=>long_address2,
		out_data=>out_data_byte1,
		out_data2=>out_data2_byte1		
		);
		
	block2: ram_block port map (
		clk=>clk,
		we=>we_byte2,
		we2=>we2_byte2,
		in_data=>in_data_byte2,
		in_data2=>in_data2_byte2,
		in_address=>long_address,
		in_address2=>long_address2,
		out_data=>out_data_byte2,
		out_data2=>out_data2_byte2		
		);
		
	block3: ram_block port map (
		clk=>clk,
		we=>we_byte3,
		we2=>we2_byte3,
		in_data=>in_data_byte3,
		in_data2=>in_data2_byte3,
		in_address=>long_address,
		in_address2=>long_address2,
		out_data=>out_data_byte3,
		out_data2=>out_data2_byte3		
		);
	

	out_data <= out_data_byte3 & out_data_byte2 & out_data_byte1 & out_data_byte0;
	
	-- out muxes
	out_byte2 <= out_data2_byte0 when shadow_address2 = "00" else
							out_data2_byte1 when shadow_address2 = "01" else
							out_data2_byte2 when shadow_address2 = "10" else
							out_data2_byte3;
	
	out_word2 <= out_data2_byte1 & out_data2_byte0 when shadow_address2(1) = '0' else
							out_data2_byte3 & out_data2_byte2;
	
	
	out_data2<=out_data2_byte3 & out_data2_byte2 & out_data2_byte1 & out_data2_byte0 when shadow_mode2 = long else
						zero_word & out_word2 when shadow_mode2 = word else
						zero_byte & out_byte2 when shadow_mode2 = byte else
						zero_byte & shadow_uart;
							
end data_ram_arc;
