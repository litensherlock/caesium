
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity steg2 is
  port(clk : in std_logic;
			 rst : in std_logic;
       constant_to_im2 : in std_logic_vector(15 downto 0);
       regs_data_in1 : in std_logic_vector(31 downto 0);
       regs_data_in2 : in std_logic_vector(31 downto 0);
			 destination_register_ir3 : in std_logic_vector(4 downto 0);
			 destination_register_ir4 : in std_logic_vector(4 downto 0);
			 OP_ir3 : in std_logic_vector (5 downto 0);
			 OP_ir4 : in std_logic_vector (5 downto 0);
       dataforward_from_steg3 : in std_logic_vector(31 downto 0);
       dataforward_from_steg4 : in std_logic_vector(31 downto 0);
			 OP_in : in std_logic_vector (5 downto 0);
			 areg_in : in std_logic_vector (4 downto 0);
			 breg_in : in std_logic_vector (4 downto 0);
			 dreg_in : in std_logic_vector (4 downto 0);
			 iur : in std_logic;
			 our : in std_logic;
			 pc_from_steg1 : in std_logic_vector(13 downto 0);
			 internal_nop  : in std_logic;
			 reg0_in : in std_logic_vector(13 downto 0);
       
			 constant_out : out std_logic_vector(15 downto 0);
			 pc_current_out : out std_logic_vector(13 downto 0);
			 address_to_instruction_memory : out std_logic_vector(13 downto 0);
			 OP_out : out std_logic_vector (5 downto 0);
			 dreg_out : out std_logic_vector (4 downto 0);
			 breg_out : out std_logic_vector (4 downto 0);
       alu_to_steg3 : out std_logic_vector(31 downto 0)
       );
end steg2;

architecture steg2_arc of steg2 is

  signal dataforwarded_address_steg3 : std_logic_vector(15 downto 0);
  signal dataforwarded_address_steg4 : std_logic_vector(15 downto 0);
  signal chosen_address : std_logic_vector(15 downto 0);
  
  signal im2 : std_logic_vector(15 downto 0);
	signal OP_ir2 : std_logic_vector (5 downto 0);
	signal areg : std_logic_vector (4 downto 0);
	signal breg : std_logic_vector (4 downto 0);
	
	signal pc_next_value,pc_from_steg1_delayed : std_logic_vector(13 downto 0);
  signal pc_current,reg0 : std_logic_vector(13 downto 0);
  
  signal alu2_in_steg2 : std_logic_vector(31 downto 0);
    
  signal chosen_reg1 : std_logic_vector(31 downto 0);
  signal chosen_reg2 : std_logic_vector(31 downto 0);

  signal alu_value : std_logic_vector(32 downto 0);

  signal z_flag : std_logic;
  signal n_flag : std_logic;
  signal c_flag : std_logic;
	signal v_flag : std_logic;

  signal steg3_writes_to_register, steg4_writes_to_register: BOOLEAN;
  signal steg2_reads_from_register_1, steg2_reads_from_register_2: BOOLEAN;
  signal steg3_to_reg, steg4_to_reg: STD_LOGIC_VECTOR(4 downto 0);
	
	-- constants
	constant ADD_r :std_logic_vector (5 downto 0) := "000001";
	constant ADD_c :std_logic_vector (5 downto 0) := "000010";
	constant SUB_r :std_logic_vector (5 downto 0) := "000011";
	constant SUB_c :std_logic_vector (5 downto 0) := "000100";
	constant LOAD :std_logic_vector (5 downto 0) := "000101";
	constant STORE :std_logic_vector (5 downto 0) := "000110";
	constant CMP_r :std_logic_vector (5 downto 0) := "000111";
	constant JMP :std_logic_vector (5 downto 0) := "001000";
	constant JOour :std_logic_vector (5 downto 0) := "010001";
	constant JOZ :std_logic_vector (5 downto 0) := "001010";
	constant JNZ :std_logic_vector (5 downto 0) := "001011";
	constant JOC :std_logic_vector (5 downto 0) := "001100";
	constant JNC :std_logic_vector (5 downto 0) := "001101";
	constant JON :std_logic_vector (5 downto 0) := "001110";
	constant JNN :std_logic_vector (5 downto 0) := "001111";
	constant JOV :std_logic_vector (5 downto 0) := "010010";
	constant JNV :std_logic_vector (5 downto 0) := "010011";
	constant JOiur :std_logic_vector (5 downto 0) := "010100";
	constant JNiur :std_logic_vector (5 downto 0) := "010101";		
	
	constant JNour :std_logic_vector (5 downto 0) := "011000";
	constant LSR :std_logic_vector (5 downto 0) := "011001";
	constant ASR :std_logic_vector (5 downto 0) := "011010";
	constant XSL :std_logic_vector (5 downto 0) := "011011";
	constant CNTRL :std_logic_vector (5 downto 0) := "011100";
	constant AND_r :std_logic_vector (5 downto 0) := "011101";
	constant OR_r :std_logic_vector (5 downto 0) := "011110";
	constant add_c_no_flags :std_logic_vector (5 downto 0) := "011111";
	constant push_pc :std_logic_vector (5 downto 0) := "100000";
	
	constant pop_pc :std_logic_vector (5 downto 0) := "100010";
	constant xor_r :std_logic_vector (5 downto 0) := "100011";
	constant sub_c_no_flags :std_logic_vector (5 downto 0) := "100100";
	constant CMP_c :std_logic_vector (5 downto 0) := "100101";
	constant CLEAR :std_logic_vector (5 downto 0) := "100110";
	constant MOVE :std_logic_vector (5 downto 0) := "100111";
	constant PUT :std_logic_vector (5 downto 0) := "101000";
	constant GET :std_logic_vector (5 downto 0) := "101001";
	constant put_b :std_logic_vector (5 downto 0) := "101010";
	constant get_b :std_logic_vector (5 downto 0) := "101011";
	constant store_b :std_logic_vector (5 downto 0) := "101100";
	constant load_b :std_logic_vector (5 downto 0) := "101101";
	constant put_w :std_logic_vector (5 downto 0) := "101110";
	constant get_w :std_logic_vector (5 downto 0) := "101111";
	constant store_w :std_logic_vector (5 downto 0) := "110000";
	constant load_w :std_logic_vector (5 downto 0) := "110001";	
	constant JMP_r :std_logic_vector (5 downto 0) := "110010";
	constant joz_r :std_logic_vector (5 downto 0) := "110011";
	constant Jnz_r :std_logic_vector (5 downto 0) := "110100";
	constant Joc_r :std_logic_vector (5 downto 0) := "110101";
	constant Jnc_r :std_logic_vector (5 downto 0) := "110110";
	constant Jon_r :std_logic_vector (5 downto 0) := "110111";
	constant Jnn_r :std_logic_vector (5 downto 0) := "111000";
	constant Jov_r :std_logic_vector (5 downto 0) := "111001";
	constant Jnv_r :std_logic_vector (5 downto 0) := "111011";
	constant Joiur_r :std_logic_vector (5 downto 0) := "111100";
	constant Jniur_r :std_logic_vector (5 downto 0) := "111101";
	constant Joour_r :std_logic_vector (5 downto 0) := "111110";
	constant Jnour_r :std_logic_vector (5 downto 0) := "111111";
	
	--more constants
	constant long : std_logic_vector (1 downto 0) := "00";
	constant word : std_logic_vector (1 downto 0) := "01";
	constant byte : std_logic_vector (1 downto 0) := "10";
	
begin
	OP_out<=OP_ir2;
	constant_out <= im2;
	breg_out<=breg;
	pc_current_out<=pc_current;
  
--IM2. Klockat register. Innehåller konstanten som används i vissa fall i steg 2. t.ex reg + konstant => reg.
  process(clk)
  begin
    if rising_edge(clk) then
      im2 <= constant_to_im2;
    end if;
  end process;


--IR2. Klockat register för instruktionen i steg 2.
  process(clk)
  begin
    if rising_edge(clk) then      
			OP_ir2<= OP_in;
			areg<= areg_in;
			breg<= breg_in;
			dreg_out<=dreg_in;
    end if;
  end process;
	
	
-- Denna process kopierar instruktionerna i steg2-4 samt nuvarande pc, då dessa behövs för olika operationer utanför stegen.
  process(clk)
  begin
    if rising_edge(clk) then
      pc_from_steg1_delayed <= pc_from_steg1;
    end if;
  end process;
	
-- MUX TILL PC
-- Väljer vilket värde pc ska få, vanligast är pc + 1. Utöver det kan vi även behöva nollställa pc, skicka in det värde som vi har fått från steg 4 och dataminnet vid instruktionen POPPC,
-- eller senaste pc värde, som används vid hopp, för att inte utföra instruktioner efter hopp.
  pc_next_value <= "00000000000000" when rst = '1' else
                   pc_current when internal_nop ='1' else 
                   reg0 when OP_ir2 = jmp_r or
									 (OP_ir2 = joz_r and z_flag = '1') or 
									 (OP_ir2 = jnz_r and z_flag = '0') or 
									 (OP_ir2 = joc_r and c_flag = '1') or 
									 (OP_ir2 = jnc_r and c_flag = '0') or 
									 (OP_ir2 = jon_r and n_flag = '1') or 
									 (OP_ir2 = jnn_r and n_flag = '0') or 
									 (OP_ir2 = jov_r and v_flag = '1') or 
									 (OP_ir2 = jnv_r and v_flag = '0') else 
                   pc_from_steg1_delayed when (OP_ir2 = JMP) or
                   (OP_ir2 = JOZ and z_flag = '1') or
                   (OP_ir2 = JNZ and z_flag = '0') or
                   (OP_ir2 = JOC and c_flag = '1') or
                   (OP_ir2 = JNC and c_flag = '0') or
                   (OP_ir2 = JON and n_flag = '1') or
                   (OP_ir2 = JNN and n_flag = '0') or	
									 (OP_ir2 = JOV and v_flag = '1') or
                   (OP_ir2 = JNV and v_flag = '0') or
									 (OP_ir2 = JOiur and iur = '1') or
                   (OP_ir2 = JNiur and iur = '0') or 
									 (OP_ir2 = JOour and our = '1') or
                   (OP_ir2 = JNour and our = '0') else
                   pc_current +1;
	
 -- PC, klockat register för nuvarande pc värde.
  process(clk)
  begin
    if rising_edge(clk) then
      pc_current <= pc_next_value;
    end if;
  end process;
	
  address_to_instruction_memory <= pc_next_value;   
-- MUX mellan IM2 och data från reg2 mot alu.
  with OP_ir2 select
    alu2_in_steg2 <= "0000000000000000" & im2 when ADD_c|sub_c|add_c_no_flags|sub_c_no_flags|CMP_c|push_pc,
    regs_data_in2 when others;

	reg0<= dataforward_from_steg3(13 downto 0) when steg3_writes_to_register and destination_register_ir3=0 else
					dataforward_from_steg4(13 downto 0) when steg4_writes_to_register and destination_register_ir4=0 else
					reg0_in;
  --****************************
  -- Dataforwardinglogik.
  -- Steg 3 skriver till ett register om den innehåller någon av dessa opkoder.
  -- LDA utesluts med flit.

  with OP_ir3 select
    steg3_writes_to_register <= true when ADD_r|ADD_c|SUB_r|sub_c|LSR|ASR|XSL|AND_r|OR_r|xor_r|CLEAR|MOVE|add_c_no_flags|sub_c_no_flags,
    false when others;
  
  
  -- Steg 4:s villkor innehåller samma opkoder och Load/get eftersom dessa är klara i
  -- steg 4.

  with OP_ir4 select
    steg4_writes_to_register <= true when ADD_r|ADD_c|SUB_r|sub_c|LOAD|load_b|load_w|LSR|ASR|XSL|AND_r|OR_r|xor_r|CLEAR|MOVE|GET|get_b|add_c_no_flags|sub_c_no_flags,
    false when others;
  
  
  -- Är jag en instruktion som har ett register 1-fält? (Exempel: add reg + konst
  -- -> reg läser från reg1 och är därför med.)

  with OP_ir2 select
    steg2_reads_from_register_1 <= true when ADD_r|ADD_c|SUB_r|sub_c|STORE|store_b|store_w|CMP_r|LSR|ASR|XSL|AND_r|
										OR_r|xor_r|CMP_c|MOVE|PUT|put_b|put_w|GET|get_b|get_w|add_c_no_flags|sub_c_no_flags,
    false when others;
  
  -- Har jag ett register 2-fält? (Exempel: add reg + reg -> reg läser både
  -- från reg1 och reg2 och är därför med.)

  -- Obs: alla push- och pop-instruktioner samt PUT och GET ingår här.
  with OP_ir2 select
    steg2_reads_from_register_2 <= true when ADD_r|SUB_r|CMP_r|AND_r| OR_r|xor_r|PUT|put_b|put_w|push_pc|pop_pc|GET|get_b|get_w,
    false when others;

  
  -- Om steg 3 skriver till ett register, jag läser från register ett, OCH de
  -- är samma register, då dataforwardas det från steg 3.
  chosen_reg1 <= dataforward_from_steg3 when steg3_writes_to_register and steg2_reads_from_register_1 
								 and destination_register_ir3 = areg else
                 -- ANNARS görs samma koll på steg fyra...
                 dataforward_from_steg4 when steg4_writes_to_register and steg2_reads_from_register_1 
								 and destination_register_ir4 = areg else
                 -- ANNARS tar vi indatat från registren.
                 regs_data_in1;

  -- Om steg 3 skriver till ett register, jag läser från register ett, OCH de
  -- är samma register, då dataforwardas det från steg 3.
  chosen_reg2 <= dataforward_from_steg3 when steg3_writes_to_register and steg2_reads_from_register_2 
								and destination_register_ir3 = breg else
                 -- ANNARS görs samma koll på steg fyra...
                 dataforward_from_steg4 when steg4_writes_to_register and steg2_reads_from_register_2 
								 and destination_register_ir4 = breg else
                 -- ANNARS tar vi indatat från register 2 eller konstanten (har
                 -- redan valts).
                 alu2_in_steg2;


  -- Detta är vår ALU i steg 2. Denna läser OP-koden i nuvarande instruktion,
  -- som säger åt den vilken matematisk operation som skall utföras. Jag anser
  -- att de flesta förklarar sig själva. Den speciella är PUSHPC.
  -- PUSHPC skickar ut det pc värde som kommer in från konstant-"platsen",

  with OP_ir2 select
    -- add reg + reg -> reg (tar två register)
    -- add reg + konstant -> reg (tar register och konstant)
    -- add reg + konstant -> reg (som inte påverkar flaggor)
    alu_value <= (('0' & chosen_reg1) + ('0' & chosen_reg2)) when ADD_r|ADD_c|add_c_no_flags,
    -- sub reg - reg -> reg (tar två register)
    -- sub reg - konstant -> reg (tar register och konstant)
    -- sub reg - konstant -> reg (som inte påverkar flaggor)
    ('0' & chosen_reg1) - chosen_reg2 when SUB_r|sub_c|sub_c_no_flags,
    -- cmp reg, reg -> flaggor (tar två register) SAMT
    -- cmp reg, konstant -> flaggor (ett register, en flagga)
    ('0' & chosen_reg1) - chosen_reg2 when CMP_r|CMP_c,
    -- LSR (ett register)
    "00" & chosen_reg1(31 downto 1) when LSR,
    -- ASR (ett register)
    '0' & chosen_reg1(31) & chosen_reg1(31 downto 1) when ASR,
    -- XSL (ett register)
    chosen_reg1(31 downto 0) & '0' when XSL,
    -- AND (register, register)
    (('0' & chosen_reg1) and ('0' & chosen_reg2)) when AND_r,
    -- OR (register, register)
    (('0' & chosen_reg1) or ('0' & chosen_reg2)) when OR_r,
    -- push PC (endast konstant)
    '0' & chosen_reg2 when push_pc,
    
    "000000000000000000000000000000000" when CLEAR,
		'0' & (chosen_reg1 xor chosen_reg2) when xor_r,
    -- Släpp igenom register 1 i alla andra fall.
    '0' & chosen_reg1 when others;
  
  -- Denna process är rent bildligt en del av ALUn, den uppdaterar flaggorna
  -- beroende på alu_value samt kollar så att den instruktion som utfördes
  -- faktiskt är en sådan som vill uppdatera flaggorna.
  process(clk)
  begin
    if rising_edge(clk) then           
      if OP_ir2/=add_c_no_flags and 
			OP_ir2/=sub_c_no_flags and
			OP_ir2/="100000" and
			OP_ir2/="100010" then

        if alu_value = 0 then
          z_flag <= '1';
        else
          z_flag <= '0';
        end if;

        if alu_value(31) = '1' then
          n_flag <= '1';
        else
          n_flag <= '0';
        end if;

        if alu_value(32) = '1' then
          c_flag <= '1';
        else
          c_flag <= '0';
        end if;
				
				if OP_ir2 = ADD_r or OP_ir2 = ADD_c then	
					if chosen_reg1(31) = chosen_reg2(31) and chosen_reg2(31) /= alu_value(31) then
						v_flag<= '1';
					else
						v_flag <='0';
					end if;
				else
					if chosen_reg1(31) /= chosen_reg2(31) and chosen_reg2(31) = alu_value(31) then
						v_flag<= '1';				
					else
						v_flag<='0';
					end if;
				end if;
				
      end if;
    end if;
  end process;
 
  alu_to_steg3 <= alu_value(31 downto 0);
  
end steg2_arc;