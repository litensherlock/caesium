library IEEE;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.STD_LOGIC_1164.ALL;

entity REGS is
  port(clk, we, we_jstk, stall_from_cpu: in std_logic;
       rst: in STD_LOGIC;
       data3_in, data_jstk_in: in STD_LOGIC_VECTOR(31 downto 0);
       addr1, addr2, addr3: in STD_LOGIC_VECTOR(4 downto 0);
       
       data1_out, data2_out: out STD_LOGIC_VECTOR(31 downto 0);     
			 reg0_out : out std_logic_vector(13 downto 0);

       pal0, pal1, pal2, pal3: out std_logic_vector(7 downto 0));
end REGS;

architecture REGS_arc of REGS is
  type regs is array (0 to 31) of std_logic_vector(31 downto 0);
  signal reg : regs;

  constant jstk_reg: INTEGER := 27;
  
-- F�r paletterna.
  signal reg28, reg29, reg30, reg31: STD_LOGIC_VECTOR(31 downto 0);
begin  
  reg28 <= reg(28);
  
  pal0 <= reg28(31 downto 24);
  pal1 <= reg28(23 downto 16);
  pal2 <= reg28(15 downto 8);
  pal3 <= reg28(7 downto 0);
	reg0_out<= reg(0)(13 downto 0);

  process (clk)
  begin
    if rising_edge(clk) then
      if rst = '1' then
        -- En synkron reset-signal utifr�n kan t�mma alla registren.
        -- Detta �r mest anv�ndbart n�r man laddar flera program i snabb f�ljd
        -- f�r att testa datorn.
        for i in 0 to 31 loop
          reg(i) <= "00000000000000000000000000000000";
        end loop;
      elsif (we = '1') then
        reg(to_integer(unsigned(addr3))) <= data3_in;
      end if;

      if (we_jstk = '1') then
        reg(jstk_reg) <= data_jstk_in;
      end if;

      -- Om stall �r P� (inversen av vad den h�r if-satsen s�ger) kommer vi
      -- INTE �ndra utv�rdena.
      if stall_from_cpu = '0' then
        -- Om v�rdet som skall l�sas i n�got av fallen �r samma som vi skriver
        -- till i den h�r klockcykeln "dataforwardar" vi ut v�rdet som skrivs i
        -- st�llet.
        if addr3 = addr1 and we = '1' then
          data1_out <= data3_in;
        else
          data1_out <= reg(to_integer(unsigned(addr1)));
        end if;

        if addr3 = addr2 and we = '1' then
          data2_out <= data3_in;
        else
          data2_out <= reg(to_integer(unsigned(addr2)));
        end if;
      end if;
      
    end if;
  end process;
end REGS_arc;
