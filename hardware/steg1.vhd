
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity steg1 is
  port(clk : in std_logic;
       instruction_in : in std_logic_vector(31 downto 0);
       pc_in_steg1 : in std_logic_vector(13 downto 0);
       
       instruction_out : out std_logic_vector(31 downto 0);

       constant_to_steg2 : out std_logic_vector(15 downto 0);
       address_to_regs1 : out std_logic_vector(4 downto 0);
       address_to_regs2 : out std_logic_vector(4 downto 0);
       pc_out_steg1 : out std_logic_vector(13 downto 0);
       joystick_enable : out std_logic     
       );
end steg1;

architecture steg1_arc of steg1 is
  signal current_instruction_steg1 : std_logic_vector(31 downto 0);
	
	-- constants
	constant ADD_r :std_logic_vector (5 downto 0) := "000001";
	constant ADD_c :std_logic_vector (5 downto 0) := "000010";
	constant SUB_r :std_logic_vector (5 downto 0) := "000011";
	constant SUB_c :std_logic_vector (5 downto 0) := "000100";
	constant LOAD :std_logic_vector (5 downto 0) := "000101";
	constant STORE :std_logic_vector (5 downto 0) := "000110";
	constant CMP_r :std_logic_vector (5 downto 0) := "000111";
	constant JMP :std_logic_vector (5 downto 0) := "001000";
	constant JOour :std_logic_vector (5 downto 0) := "010001";
	constant JOZ :std_logic_vector (5 downto 0) := "001010";
	constant JNZ :std_logic_vector (5 downto 0) := "001011";
	constant JOC :std_logic_vector (5 downto 0) := "001100";
	constant JNC :std_logic_vector (5 downto 0) := "001101";
	constant JON :std_logic_vector (5 downto 0) := "001110";
	constant JNN :std_logic_vector (5 downto 0) := "001111";
	constant JOA :std_logic_vector (5 downto 0) := "010000";
	constant JNA :std_logic_vector (5 downto 0) := "010001";	
	constant JOV :std_logic_vector (5 downto 0) := "010010";
	constant JNV :std_logic_vector (5 downto 0) := "010011";
	constant JOiur :std_logic_vector (5 downto 0) := "010100";
	constant JNiur :std_logic_vector (5 downto 0) := "010101";		
	
	constant JNour :std_logic_vector (5 downto 0) := "011000";
	constant LSR :std_logic_vector (5 downto 0) := "011001";
	constant ASR :std_logic_vector (5 downto 0) := "011010";
	constant XSL :std_logic_vector (5 downto 0) := "011011";
	constant CNTRL :std_logic_vector (5 downto 0) := "011100";
	constant AND_r :std_logic_vector (5 downto 0) := "011101";
	constant OR_r :std_logic_vector (5 downto 0) := "011110";
	constant add_c_no_flags :std_logic_vector (5 downto 0) := "011111";
	constant push_pc :std_logic_vector (5 downto 0) := "100000";
	
	constant pop_pc :std_logic_vector (5 downto 0) := "100010";
	constant xor_r :std_logic_vector (5 downto 0) := "100011";
	constant sub_c_no_flags :std_logic_vector (5 downto 0) := "100100";
	constant CMP_c :std_logic_vector (5 downto 0) := "100101";
	constant CLEAR :std_logic_vector (5 downto 0) := "100110";
	constant MOVE :std_logic_vector (5 downto 0) := "100111";
	constant PUT :std_logic_vector (5 downto 0) := "101000";
	constant GET :std_logic_vector (5 downto 0) := "101001";
	constant put_b :std_logic_vector (5 downto 0) := "101010";
	constant get_b :std_logic_vector (5 downto 0) := "101011";
	constant store_b :std_logic_vector (5 downto 0) := "101100";
	constant load_b :std_logic_vector (5 downto 0) := "101101";
	constant put_w :std_logic_vector (5 downto 0) := "101110";
	constant get_w :std_logic_vector (5 downto 0) := "101111";
	constant store_w :std_logic_vector (5 downto 0) := "110000";
	constant load_w :std_logic_vector (5 downto 0) := "110001";	
	constant JMP_r :std_logic_vector (5 downto 0) := "110010";
	constant joz_r :std_logic_vector (5 downto 0) := "110011";
	constant Jnz_r :std_logic_vector (5 downto 0) := "110100";
	constant Joc_r :std_logic_vector (5 downto 0) := "110101";
	constant Jnc_r :std_logic_vector (5 downto 0) := "110110";
	constant Jon_r :std_logic_vector (5 downto 0) := "110111";
	constant Jnn_r :std_logic_vector (5 downto 0) := "111000";
	constant Jov_r :std_logic_vector (5 downto 0) := "111001";
	constant Jnv_r :std_logic_vector (5 downto 0) := "111011";
	constant Joiur_r :std_logic_vector (5 downto 0) := "111100";
	constant Jniur_r :std_logic_vector (5 downto 0) := "111101";
	constant Joour_r :std_logic_vector (5 downto 0) := "111110";
	constant Jnour_r :std_logic_vector (5 downto 0) := "111111";
	
begin
  instruction_out <= current_instruction_steg1;
  
  -- Klockat register för instruktionen i steg 1.
  process(clk)
  begin
    if rising_edge(clk) then
      current_instruction_steg1 <= instruction_in; 
    end if;
  end process;


  
-- Om instruktionen är CNTRL, så sätts joystick_enable till 1. vilket skickar
-- en 1a till joysticken som säger åt den att börja skicka ut sitt värde.
  with current_instruction_steg1(31 downto 26) select
    joystick_enable <= '1' when CNTRL,
    '0' when others;
  



  -- Skickar ut addresser till registret. Detta händer alltid. Vi hanterar i
  -- steg2 de fall där dessa ej används.
  
  address_to_regs1 <= current_instruction_steg1(25 downto 21);
  address_to_regs2 <= current_instruction_steg1(20 downto 16);

-- här skickas konstanten i instruktionen vidare till steg2, i alla fall
-- förutom PUSHPC, där istället det nuvarande PC värdet skickas ut.
  with current_instruction_steg1(31 downto 26) select
    constant_to_steg2 <= "00" & pc_in_steg1 when push_pc,
    current_instruction_steg1(20 downto 5) when others;
  

  -- Skickar ut ett nytt värde till pc som används vid jump instruktioner

    pc_out_steg1 <= current_instruction_steg1(18 downto 5);
  
  
end steg1_arc;