
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity steg4 is
  port(clk : in std_logic;
       OP_in : in std_logic_vector(5 downto 0);
			 dreg_in : in std_logic_vector(4 downto 0);
       data_from_data_memory : in std_logic_vector(31 downto 0);
       data_from_steg3_to_d4 : in std_logic_vector(31 downto 0);
       address_to_regs3 : out std_logic_vector(4 downto 0);
       data_to_regs : out std_logic_vector(31 downto 0);
       write_enable_to_regs : out std_logic;
       current_OP : out std_logic_vector(5 downto 0);
       dataforward_steg4 : out std_logic_vector(31 downto 0)
       );
end steg4;

architecture steg4_arc of steg4 is

  signal current_d4 : std_logic_vector(31 downto 0);
  signal data_from_mux : std_logic_vector(31 downto 0);
	signal OP : std_logic_vector(5 downto 0);
	signal destination_register : std_logic_vector(4 downto 0);
	
	-- constants
	constant ADD_r :std_logic_vector (5 downto 0) := "000001";
	constant ADD_c :std_logic_vector (5 downto 0) := "000010";
	constant SUB_r :std_logic_vector (5 downto 0) := "000011";
	constant SUB_c :std_logic_vector (5 downto 0) := "000100";
	constant LOAD :std_logic_vector (5 downto 0) := "000101";
	constant STORE :std_logic_vector (5 downto 0) := "000110";
	constant CMP_r :std_logic_vector (5 downto 0) := "000111";
	constant JMP :std_logic_vector (5 downto 0) := "001000";
	constant JOour :std_logic_vector (5 downto 0) := "010001";
	constant JOZ :std_logic_vector (5 downto 0) := "001010";
	constant JNZ :std_logic_vector (5 downto 0) := "001011";
	constant JOC :std_logic_vector (5 downto 0) := "001100";
	constant JNC :std_logic_vector (5 downto 0) := "001101";
	constant JON :std_logic_vector (5 downto 0) := "001110";
	constant JNN :std_logic_vector (5 downto 0) := "001111";
	constant JOA :std_logic_vector (5 downto 0) := "010000";
	constant JNA :std_logic_vector (5 downto 0) := "010001";	
	constant JOV :std_logic_vector (5 downto 0) := "010010";
	constant JNV :std_logic_vector (5 downto 0) := "010011";
	constant JOiur :std_logic_vector (5 downto 0) := "010100";
	constant JNiur :std_logic_vector (5 downto 0) := "010101";		
	
	constant JNour :std_logic_vector (5 downto 0) := "011000";
	constant LSR :std_logic_vector (5 downto 0) := "011001";
	constant ASR :std_logic_vector (5 downto 0) := "011010";
	constant XSL :std_logic_vector (5 downto 0) := "011011";
	constant CNTRL :std_logic_vector (5 downto 0) := "011100";
	constant AND_r :std_logic_vector (5 downto 0) := "011101";
	constant OR_r :std_logic_vector (5 downto 0) := "011110";
	constant add_c_no_flags :std_logic_vector (5 downto 0) := "011111";
	constant push_pc :std_logic_vector (5 downto 0) := "100000";
	
	constant pop_pc :std_logic_vector (5 downto 0) := "100010";
	constant xor_r :std_logic_vector (5 downto 0) := "100011";
	constant sub_c_no_flags :std_logic_vector (5 downto 0) := "100100";
	constant CMP_c :std_logic_vector (5 downto 0) := "100101";
	constant CLEAR :std_logic_vector (5 downto 0) := "100110";
	constant MOVE :std_logic_vector (5 downto 0) := "100111";
	constant PUT :std_logic_vector (5 downto 0) := "101000";
	constant GET :std_logic_vector (5 downto 0) := "101001";
	constant put_b :std_logic_vector (5 downto 0) := "101010";
	constant get_b :std_logic_vector (5 downto 0) := "101011";
	constant store_b :std_logic_vector (5 downto 0) := "101100";
	constant load_b :std_logic_vector (5 downto 0) := "101101";
	constant put_w :std_logic_vector (5 downto 0) := "101110";
	constant get_w :std_logic_vector (5 downto 0) := "101111";
	constant store_w :std_logic_vector (5 downto 0) := "110000";
	constant load_w :std_logic_vector (5 downto 0) := "110001";	
	constant JMP_r :std_logic_vector (5 downto 0) := "110010";
	constant joz_r :std_logic_vector (5 downto 0) := "110011";
	constant Jnz_r :std_logic_vector (5 downto 0) := "110100";
	constant Joc_r :std_logic_vector (5 downto 0) := "110101";
	constant Jnc_r :std_logic_vector (5 downto 0) := "110110";
	constant Jon_r :std_logic_vector (5 downto 0) := "110111";
	constant Jnn_r :std_logic_vector (5 downto 0) := "111000";
	constant Jov_r :std_logic_vector (5 downto 0) := "111001";
	constant Jnv_r :std_logic_vector (5 downto 0) := "111011";
	constant Joiur_r :std_logic_vector (5 downto 0) := "111100";
	constant Jniur_r :std_logic_vector (5 downto 0) := "111101";
	constant Joour_r :std_logic_vector (5 downto 0) := "111110";
	constant Jnour_r :std_logic_vector (5 downto 0) := "111111";
  
begin
  current_op <= OP;
  address_to_regs3 <= destination_register;
  dataforward_steg4 <= data_from_mux; 
  data_to_regs <= data_from_mux;
  
  -- Klockat register för instruktionen i steg 4 och det data som kommer in från steg3.
  process(clk)
  begin
    if rising_edge(clk) then
      OP <= OP_in;
			destination_register<=dreg_in;
      current_d4 <= data_from_steg3_to_d4;
    end if;
  end process;

  -- Denna väljer vilken signal som skall skickas vidare till registren,
  -- beroende på instruktion kan detta vara antingen data från dataminnet eller
  -- data från steg3, som innan dess kommit från alu i steg 2.
  with OP select
    data_from_mux <= data_from_data_memory when LOAD|load_w|load_b|GET|get_w|get_b|pop_pc,
    current_d4 when others;
  

-- skickar write enable till register vid de fallen där vi faktiskt skall
-- skriva till dom, beror på instruktionen som ligger i steg4.
  with OP select
    write_enable_to_regs <= '1' when ADD_r|ADD_c|SUB_r|SUB_c|LOAD|load_w|load_b|LSR|ASR|XSL|AND_r|OR_r
																		 |xor_r|CLEAR|MOVE|GET|get_w|get_b|add_c_no_flags|sub_c_no_flags,
    '0' when others;
  
end steg4_arc;
