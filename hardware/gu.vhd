library IEEE;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.STD_LOGIC_1164.ALL;

entity GU is
  port (clk, rst: in STD_LOGIC;
        -- hsync och vsync är VGA-kontrollsignaler enligt VGA-standard.
        hsync, vsync, leaving_screen_interrupt: out STD_LOGIC;
        -- Åtta bitar färg som går ut på kortets VGA-port.
        VGA: out STD_LOGIC_VECTOR(7 downto 0);
        -- Adress till minnet.
        address: out STD_LOGIC_VECTOR(13 downto 0);

        -- Data från minnet.
        data: in STD_LOGIC_VECTOR(31 downto 0);

        -- Sexton paletter från registerfilen, eller varsomhelstifrån
        -- egentligen, vi bryr oss inte i den här komponenten.
        pal0, pal1, pal2, pal3, pal4, pal5, pal6, pal7, pal8, pal9, pal10, pal11, pal12, pal13, pal14, pal15: in STD_LOGIC_VECTOR(7 downto 0));
end GU;

architecture new_graphics_unit of GU is

  -- Addresserna till de tre laddningarna vi utför.
  signal tile_location_address, tile_pixel_address, pixel_address: STD_LOGIC_VECTOR(13 downto 0);
  
  -- Den faktiska pixeln 0->799 som vi är på i x-led och vad den kommer vara i
  -- nästa dubbelpixel.
  signal x_actual, x_next_actual: STD_LOGIC_VECTOR(9 downto 0);

  -- Samma för Y.
  signal y_actual, y_next_actual: STD_LOGIC_VECTOR(9 downto 0);

  -- Vilken av våra dubbelpixlar (0->239) som vi laddar respektive är på i x-led.
  signal x_next_apparent, x_apparent: UNSIGNED(8 downto 0);

  -- Samma i y-led.
  signal y_next_apparent, y_apparent: UNSIGNED(8 downto 0);

  type direction_states is (DRAWING, FRONT_PORCH, SYNC, BACK_PORCH);

  -- Vilket av dessa tillstånd vi är på och kommer vara på i x- och y-led.
  signal x_state, x_next_state, y_state, y_next_state: direction_states;

  type within_pixel_states is (FIRST_DRAW, WAIT1, LOAD_WHICH_TILE, ADDR_PIXEL_IN_TILE, WAIT2, LOAD_TILE_PIXEL, WAIT3, COUNTUP);
  signal within_pixel_state: within_pixel_states;

  -- Är vi i ett av hörnen i en tile?
  signal corner: STD_LOGIC;

  -- Ritar vi alls i nästa tillstånd?
  signal drawing_next_pixel: BOOLEAN;

  -- Färgen som vi ritar ut (färgen i koord. (x_apparent, y_apparent)) medan vi
  -- laddar färgen i (x_next_apparent, y_next_apparent);
  signal last_palette: STD_LOGIC_VECTOR(3 downto 0);
  
  -- Hur många stycken av tio dubbelpixlar som vi har passerat i x- samt y-led.
  signal parts_of_ten_x, parts_of_ten_y: STD_LOGIC_VECTOR(4 downto 0);

  -- Räknare som används för att öka parts_of_ten-variablerna.
  signal xmodten, ymodten: UNSIGNED(3 downto 0);

  -- Vilken av våra 256 tilar är vi i?
  signal which_tile: STD_LOGIC_VECTOR(7 downto 0);
  
  -- Vilken av tilens 100 pixlar vi är på...
  signal actual_index_within_tile: UNSIGNED(6 downto 0);
  -- Och vilken av de 96 detta innebär i praktiken.
  signal corrected_index_within_tile: UNSIGNED(6 downto 0);

  signal loaded_tile_pixel: STD_LOGIC;
  signal loaded_palette: STD_LOGIC_VECTOR(3 downto 0);
  signal chosen_colour: STD_LOGIC_VECTOR(7 downto 0);

  constant start_of_pixel_buffer: STD_LOGIC_VECTOR(13 downto 0) := "01101010000000";
  -- 6784...
  constant start_of_tile_buffer: STD_LOGIC_VECTOR(13 downto 0) := "01100111000000";
  -- 6592...
  constant start_of_tiles: STD_LOGIC_VECTOR(13 downto 0) := "01011011000000";
  -- 5824...

  -- Vilken palett använder tilar?
  constant tile_palette: STD_LOGIC_VECTOR(3 downto 0) := "0010";

  -- Är vi på en ny dubbelrad eller på en upprepad falsk rad?
  signal even_row, even_row_next: STD_LOGIC;
begin

  with last_palette select
    chosen_colour <= pal0 when "0000",
    pal1 when "0001",
    pal2 when "0010",
    pal3 when "0011",
    pal4 when "0100",
    pal5 when "0101",
    pal6 when "0110",
    pal7 when "0111",
    pal8 when "1000",
    pal9 when "1001",
    pal10 when "1010",
    pal11 when "1011",
    pal12 when "1100",
    pal13 when "1101",
    pal14 when "1110",
    pal15 when "1111",
    "XXXXXXXX" when others;
  
  -- Vi skriver ut den senast laddade färgen via vad den har för palettsiffra
  -- om vi inte tvingar ut svart av något skäl.
  VGA <= "00000000" when not (x_state = DRAWING and y_state = DRAWING) else
         chosen_colour;
  
  -- Om man delar upp fönstret i 10*10-rutor och numrerar rutorna från vänster
  -- till höger, rad för rad, då kommer vår koordinat i den rutan som vi är i
  -- just nu vara (10*(y mod 10) + (x mod 10)).
  actual_index_within_tile <= resize(ymodten*8 +
                                     ymodten*2 +
                                     xmodten, 7);

  -- Vi klipper av hörnen från våra tilar (=> 96 bitar per tile, vilket precis
  -- passar på tre adresser) och därför förskjuts pixeln som vi faktiskt skall
  -- ladda med olika mycket beroende på var vi var.
  corrected_index_within_tile <= actual_index_within_tile - 1 when actual_index_within_tile < 9 else
                                 actual_index_within_tile - 2 when actual_index_within_tile < 90 else
                                 actual_index_within_tile - 3;

  -- Hörnen som vi klipper bort motsvarar exakt index 0, 9, 90 och 99. (nollindexerat)
  corner <= '1' when actual_index_within_tile = 0 or actual_index_within_tile = 9 or actual_index_within_tile = 90 or actual_index_within_tile = 99 else
            '0';

  -- Vi har 256 tilar. I vår 10*10-ruta står en av dem. tile_location_address
  -- innehåller adressen till där i minnet som det står vilken av de 256 vi har
  -- i vår ruta.
  tile_location_address <= start_of_tile_buffer + std_logic_vector(unsigned(parts_of_ten_y & "000") + unsigned(parts_of_ten_x(4 downto 2)));

  -- När vi har en siffra 0->255 som innehåller vilken tile som står i vår ruta
  -- vill vi veta vilken av de tre adresserna som denna tile består av som vi
  -- vill läsa i.
  tile_pixel_address <= start_of_tiles + std_logic_vector(unsigned("0" & which_tile & '0') + unsigned(which_tile) + corrected_index_within_tile(6 downto 5));

  -- Det står åtta pixlar i varje adress och de står i ordning, från vänster
  -- till höger, rad för rad.
  pixel_address <= start_of_pixel_buffer + std_logic_vector(resize(40*y_next_apparent + x_next_apparent/8, 14));


  -- VGA-standarden för 60 Hz, 640*480 sätter dessa siffror.
  -- Siffrorna delas med två för våra dubbelpixlar.
  x_state <= DRAWING when x_apparent < 320 else
             FRONT_PORCH when x_apparent < 328 else
             SYNC when x_apparent < 376 else
             BACK_PORCH;
  
  y_state <= DRAWING when y_apparent < 240 else
             FRONT_PORCH when y_apparent < 245 else
             SYNC when y_apparent < 246 else
             BACK_PORCH;

  x_next_state <= DRAWING when x_next_apparent < 320 else
                  FRONT_PORCH when x_next_apparent < 328 else
                  SYNC when x_next_apparent < 376 else
                  BACK_PORCH;
  y_next_state <= DRAWING when y_next_apparent < 240 else
                  FRONT_PORCH when y_next_apparent < 245 else
                  SYNC when y_next_apparent < 246 else
                  BACK_PORCH;

  drawing_next_pixel <= x_next_state = DRAWING and y_next_state = DRAWING;
  
  with x_state select
    hsync <= '1' when SYNC,
    '0' when others;
  
  with y_state select
    vsync <= '1' when SYNC,
    '0' when others;

  -- Alla tillstånd leder till ett annat tillstånd. Vid reset går vi till FIRST_DRAW.
  process (clk)
  begin
    if rising_edge(clk) then
      if rst = '1' then
        within_pixel_state <= FIRST_DRAW;
      else
        case within_pixel_state is
          when FIRST_DRAW => within_pixel_state <= WAIT1;
          when WAIT1 => within_pixel_state <= LOAD_WHICH_TILE;
          when LOAD_WHICH_TILE => within_pixel_state <= ADDR_PIXEL_IN_TILE;
          when ADDR_PIXEL_IN_TILE => within_pixel_state <= WAIT2;
          when WAIT2 => within_pixel_state <= LOAD_TILE_PIXEL;
          when LOAD_TILE_PIXEL => within_pixel_state <= WAIT3;
          when WAIT3 => within_pixel_state <= COUNTUP;
          when COUNTUP => within_pixel_state <= FIRST_DRAW;
        end case;
      end if;
    end if;
  end process;
  
  -- Räknarna räknas upp i tillståndet COUNTUP och ställs till ett bestämt
  -- värde utanför bilden vid reset.
  process (clk)
  begin
    if rising_edge(clk) then
      if rst = '1' then
        -- Börja om i slutet av en bild om vi får reset-signalen.
        -- Vi går till raden där (656, 491) laddas...

        -- 491 är udda.
        even_row <= '0';
        even_row_next <= '0';
	
        x_next_apparent <= "101001000";  -- 328, hälften av 656.
        x_apparent <= "101000111"; -- 327, ett under det.
        
        y_next_apparent <= "011110101";  -- 245, hälften av 491 avr. nedåt
        y_apparent <= "011110101"; -- Samma.

        xmodten <= "1000";              -- 8 (656 / 2 = 328)
        ymodten <= "0101";              -- 5 (491 / 2 = 245,5 => 245)

        parts_of_ten_x <= "00000";
        parts_of_ten_y <= "00000";
      elsif within_pixel_state = COUNTUP then
        -- Alla räknare räknas upp.
        if x_next_apparent >= 399 then
          -- Nolla x_next_apparent om vi når 400 pixlar i bredden.
          x_next_apparent <= "000000000";
          xmodten <= "0000";
          parts_of_ten_x <= "00000";
          
          if y_next_apparent >= 259 then
            -- Detsamma för y_next_apparent med 260 i höjden.
            if even_row_next = '1' then
              y_next_apparent <= "000000000";
              ymodten <= "0000";
              parts_of_ten_y <= "00000";
              -- 0 är en jämn rad.
              even_row_next <= '1';
            else
              even_row_next <= '1';
            end if;
            -- y_next_apparent räknas upp varje gång x_next_apparent nollas...
          elsif (even_row_next = '1') then
            y_next_apparent <= y_next_apparent + 1;
            
            if ymodten < 9 then
              ymodten <= ymodten + 1;
            else
              ymodten <= "0000";
              parts_of_ten_y <= parts_of_ten_y + 1;
            end if;
            
            even_row_next <= '0';
          else
            even_row_next <= '1';
          end if;
          
        else
          -- Om x_next_apparent inte är på en kant räknas det bara upp med ett:
          -- xmodten följer med och ökar också med ett.
          if xmodten < 9 then
            xmodten <= xmodten + 1;
          else
            xmodten <= "0000";
            parts_of_ten_x <= parts_of_ten_x + 1;
          end if;

          x_next_apparent <= x_next_apparent + 1;
        end if;

        -- Samma logik för motsvarande värden "nu".
        if x_apparent >= 399 then
          x_apparent <= "000000000";

          if y_apparent >= 259 then
            if even_row = '1' then
              y_apparent <= "000000000";
              -- 0 är en jämn rad.
              even_row <= '1';
            else
              even_row <= '1';
            end if;
          else
            if even_row = '1' then
              y_apparent <= y_apparent + 1;
              even_row <= '0';
            else
              even_row <= '1';
            end if;
          end if;
        else
          x_apparent <= x_apparent + 1;
        end if;
      end if;
    end if;
  end process;
  

  -- Avbrottet som skickas ut precis när vi lämnar skärmen skickas ut vid
  -- tilståndet FIRST_DRAW om koordinaterna (halvpixlar) är (320, 239). Detta
  -- är till höger om den högraste pixeln i buffern på den sista raden.
  -- Eftersom vi bara kollar vid FIRST_DRAW skickar vi ut avbrottet i åtta klockcykler.
  process (clk)
  begin
    if rising_edge(clk) then
      if rst = '1' then
        leaving_screen_interrupt <= '0';
      elsif within_pixel_state <= FIRST_DRAW then
        if x_next_apparent = 320 and y_next_apparent = 239 then
          leaving_screen_interrupt <= '1';
        else
          leaving_screen_interrupt <= '0';
        end if;
      end if;
    end if;
  end process;

  -- Olika adresser skickas ut vid olika tillstånd: i FIRST_DRAW skickas
  -- adressen till tilebuffern ut, i ADDR_PIXEL_IN_TILE skickas adressen till
  -- pixeln inom tilen ut och i LOAD_TILE_PIXEL skickas adressen till
  -- pixelbuffern ut.
  process (clk)
  begin
    if rising_edge(clk) then
      if rst = '1' then
        address <= "00000000000000";
      else
        case within_pixel_state is
          when FIRST_DRAW => address <= tile_location_address;
          when ADDR_PIXEL_IN_TILE => address <= tile_pixel_address;
          when LOAD_TILE_PIXEL => address <= pixel_address;
          when others => null;
        end case;
      end if;
    end if;
  end process;

  -- Det första vi laddar är "vilken tile"-siffran, som finns på data i
  -- tillståndet LOAD_WHICH_TILE. Vid reset nollar vi den.
  process(clk)
  begin
    if rising_edge(clk) then
      if rst = '1' then
        which_tile <= "00000000";
      elsif within_pixel_state = LOAD_WHICH_TILE and corner = '0' and drawing_next_pixel then
        case std_logic_vector(x_next_apparent(1 downto 0)) is
          when "00" => which_tile <= data(31 downto 24);
          when "01" => which_tile <= data(23 downto 16);
          when "10" => which_tile <= data(15 downto 8);
          when "11" => which_tile <= data(7 downto 0);
          when others => which_tile <= "XXXXXXXX";
        end case;
      end if;
    end if;
  end process;

  -- Vi laddar pixeln som detta motsvarar INOM tilen i tillståndet LOAD_TILE_PIXEL.
  -- Vid reset nollas den laddade pixeln. Den nollas också i FIRST_DRAW så att
  -- vi låtsas att vi hämtade en nolla om vi är i ett hörn.
  process (clk)
  begin
    if rising_edge(clk) then
      if rst = '1' then
        loaded_tile_pixel <= '0';
      elsif within_pixel_state = FIRST_DRAW then
        loaded_tile_pixel <= '0';
      elsif within_pixel_state = LOAD_TILE_PIXEL and corner = '0' and drawing_next_pixel then
        loaded_tile_pixel <= data(31 - to_integer(corrected_index_within_tile(4 downto 0)));
      end if;
    end if;
  end process;
  
  
  -- "last_palette" uppdateras i COUNTUP och innehåller då tile-paletten om vi
  -- laddade en etta i tile-pixeln, eller det laddade datastycket om vi laddade
  -- en nolla. Vid reset sätts den till noll.
  process (clk)
  begin
    if rising_edge(clk) then
      if rst = '1' then
        last_palette <= "0000";
      else
        if within_pixel_state = COUNTUP and drawing_next_pixel then
          if loaded_tile_pixel = '1' then
            last_palette <= tile_palette;  -- konstant.
          else
            case x_next_apparent(2 downto 0) is
              when "000" => last_palette <= data(31 downto 28);
              when "001" => last_palette <= data(27 downto 24);
              when "010" => last_palette <= data(23 downto 20);
              when "011" => last_palette <= data(19 downto 16);
              when "100" => last_palette <= data(15 downto 12);
              when "101" => last_palette <= data(11 downto 8);
              when "110" => last_palette <= data(7 downto 4);
              when "111" => last_palette <= data(3 downto 0);
              when others => last_palette <= "XXXX";
            end case;
          end if;
        end if;
      end if;
    end if;
  end process;
end;
