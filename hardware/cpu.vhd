-------------------------------------------------------------------------------
-- Det h�r �r den gamla CPU:n d�r allting �r klockat, men adress till minne och
-- write_enable till minne har flyttats till steg 2.
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity cpu is
  port(clk : in std_logic;
       reset : in std_logic;
			 iur : in std_logic;
			 our : in std_logic;
			 reg0_in : in std_logic_vector(13 downto 0);
       instruction_from_instruction_memory : in std_logic_vector(31 downto 0);	
       data_from_regs1 : in std_logic_vector(31 downto 0);
       data_from_regs2 : in std_logic_vector(31 downto 0);
       data_from_data_memory : in std_logic_vector(31 downto 0);
       address_to_instruction_memory : out std_logic_vector(13 downto 0);
       address_to_regs1 : out std_logic_vector(4 downto 0);
       address_to_regs2 : out std_logic_vector(4 downto 0);
       address_to_regs3 : out std_logic_vector(4 downto 0);
       address_to_data_memory : out std_logic_vector(15 downto 0);
       data_to_regs : out std_logic_vector(31 downto 0);
       data_to_data_memory : out std_logic_vector(31 downto 0);
       write_enable_to_regs : out std_logic;
       write_enable_to_data_memory : out std_logic;
       controll_joystick : out std_logic;	
			 mode_out : out std_logic_vector (1 downto 0)	
       );
end cpu;

architecture cpu_arc of cpu is

  component steg4 is
    port(clk : in std_logic;
       OP_in : in std_logic_vector(5 downto 0);
			 dreg_in : in std_logic_vector(4 downto 0);
       data_from_data_memory : in std_logic_vector(31 downto 0);
       data_from_steg3_to_d4 : in std_logic_vector(31 downto 0);
       address_to_regs3 : out std_logic_vector(4 downto 0);
       data_to_regs : out std_logic_vector(31 downto 0);
       write_enable_to_regs : out std_logic;
       current_OP : out std_logic_vector(5 downto 0);
       dataforward_steg4 : out std_logic_vector(31 downto 0)
       );
  end component;

  component steg3 is
    port(clk : in std_logic;
       OP_in : in std_logic_vector(5 downto 0);
			 OP_ir4_in : in std_logic_vector(5 downto 0);
			 dreg_in : in std_logic_vector(4 downto 0);
			 breg_in : in std_logic_vector(4 downto 0);
			 dreg_steg4_in : in std_logic_vector(4 downto 0);
       data_from_steg2_alu : in std_logic_vector(31 downto 0);
			 constant_in : in std_logic_vector(15 downto 0);			 
			 dataforwarded_address_steg4 : in std_logic_vector(15 downto 0);			 
       
       current_OP : out std_logic_vector(5 downto 0);
			 mode_out : out std_logic_vector (1 downto 0);	
			 write_enable_data_memory: out STD_LOGIC;
			 data_to_data_memory : out std_logic_vector(31 downto 0);
       address_to_data_memory : out std_logic_vector(15 downto 0);
			 dreg_out : out std_logic_vector(4 downto 0);
       data_to_steg4 : out std_logic_vector(31 downto 0);
       dataforward_steg3 : out std_logic_vector(31 downto 0)
       );
  end component;

  component steg2 is
    port(clk : in std_logic;
			 rst : in std_logic;
       constant_to_im2 : in std_logic_vector(15 downto 0);
       regs_data_in1 : in std_logic_vector(31 downto 0);
       regs_data_in2 : in std_logic_vector(31 downto 0);
			 destination_register_ir3 : in std_logic_vector(4 downto 0);
			 destination_register_ir4 : in std_logic_vector(4 downto 0);
			 OP_ir3 : in std_logic_vector (5 downto 0);
			 OP_ir4 : in std_logic_vector (5 downto 0);
       dataforward_from_steg3 : in std_logic_vector(31 downto 0);
       dataforward_from_steg4 : in std_logic_vector(31 downto 0);
			 OP_in : in std_logic_vector (5 downto 0);
			 areg_in : in std_logic_vector (4 downto 0);
			 breg_in : in std_logic_vector (4 downto 0);
			 dreg_in : in std_logic_vector (4 downto 0);
			 iur : in std_logic;
			 reg0_in : in std_logic_vector(13 downto 0);
			 our : in std_logic;
			 pc_from_steg1 : in std_logic_vector(13 downto 0);
			 internal_nop  : in std_logic;
       
			 constant_out : out std_logic_vector(15 downto 0);
			 pc_current_out : out std_logic_vector(13 downto 0);
			 address_to_instruction_memory : out std_logic_vector(13 downto 0);
			 OP_out : out std_logic_vector (5 downto 0);
			 dreg_out : out std_logic_vector (4 downto 0);
			 breg_out : out std_logic_vector (4 downto 0);
       alu_to_steg3 : out std_logic_vector(31 downto 0)
       );
  end component;

  component steg1 is
    port(clk : in std_logic;
         instruction_in : in std_logic_vector(31 downto 0);
         pc_in_steg1: in std_logic_vector(13 downto 0);                   
         instruction_out : out std_logic_vector(31 downto 0);
         constant_to_steg2 : out std_logic_vector(15 downto 0);
         address_to_regs1 : out std_logic_vector(4 downto 0);
         address_to_regs2 : out std_logic_vector(4 downto 0);
         pc_out_steg1 : out std_logic_vector(13 downto 0);
         joystick_enable : out std_logic
         );
  end component;

  signal pc_from_steg1,pc_current : std_logic_vector(13 downto 0);
  signal pc_next_value : std_logic_vector(13 downto 0);
  signal data_from_steg4 : std_logic_vector(31 downto 0);
  signal z_flag : std_logic;
  signal c_flag : std_logic;
  signal n_flag : std_logic;
	signal v_flag : std_logic;
  signal mux_to_ir1_check_value : std_logic_vector(1 downto 0);
  signal instruction_to_steg2 : std_logic_vector(31 downto 0);
  signal ir4_out : std_logic_vector(31 downto 0);

  
  signal instruction_to_steg1 : std_logic_vector(31 downto 0);
  
  signal ir1_to_ir2 : std_logic_vector(31 downto 0);

	signal dreg2_to_3,dreg3_to_4,breg2_to_3 : std_logic_vector(4 downto 0);
	signal OP2_to_3,OP3_to_4 : std_logic_vector(5 downto 0);	
	
  signal alu_to_d3 : std_logic_vector(31 downto 0);
  signal d3_to_d4 : std_logic_vector(31 downto 0);
	
	signal OP_ir2,OP_ir3,OP_ir4 : std_logic_vector (5 downto 0);
	signal address_to_regs3_buff : std_logic_vector (4 downto 0);

  signal dataforward_from_steg3 : std_logic_vector(31 downto 0);
  signal dataforward_from_steg4 : std_logic_vector(31 downto 0);

  signal address_to_steg3_from_steg2 : std_logic_vector(13 downto 0);

  signal pc_from_steg2_to_steg1 : std_logic_vector(13 downto 0);

  signal constant_from_steg1_to_steg2,constant2_to_3 : std_logic_vector(15 downto 0);
  
  signal internal_nop_jmp_steg1: STD_LOGIC;

  signal internal_nop : std_logic;

  signal pc_from_steg1_delayed : std_logic_vector(13 downto 0);
  
  type interrupt_states is (NORMAL_OPERATION, JSR_PUSHING_PC, JSR_REDUCING_STACK_POINTER, JSR_JUMPING, RFS_INCREASING_STACK_POINTER, RFS_POPPING_PC);
  signal interrupt_state: interrupt_states;

	-- constants
	constant ADD_r :std_logic_vector (5 downto 0) := "000001";
	constant ADD_c :std_logic_vector (5 downto 0) := "000010";
	constant SUB_r :std_logic_vector (5 downto 0) := "000011";
	constant SUB_c :std_logic_vector (5 downto 0) := "000100";
	constant LOAD :std_logic_vector (5 downto 0) := "000101";
	constant STORE :std_logic_vector (5 downto 0) := "000110";
	constant CMP_r :std_logic_vector (5 downto 0) := "000111";
	constant JMP :std_logic_vector (5 downto 0) := "001000";
	constant JOour :std_logic_vector (5 downto 0) := "010001";
	constant JOZ :std_logic_vector (5 downto 0) := "001010";
	constant JNZ :std_logic_vector (5 downto 0) := "001011";
	constant JOC :std_logic_vector (5 downto 0) := "001100";
	constant JNC :std_logic_vector (5 downto 0) := "001101";
	constant JON :std_logic_vector (5 downto 0) := "001110";
	constant JNN :std_logic_vector (5 downto 0) := "001111";
	constant JOV :std_logic_vector (5 downto 0) := "010010";
	constant JNV :std_logic_vector (5 downto 0) := "010011";
	constant JOiur :std_logic_vector (5 downto 0) := "010100";
	constant JNiur :std_logic_vector (5 downto 0) := "010101";		
	
	constant JNour :std_logic_vector (5 downto 0) := "011000";
	constant LSR :std_logic_vector (5 downto 0) := "011001";
	constant ASR :std_logic_vector (5 downto 0) := "011010";
	constant XSL :std_logic_vector (5 downto 0) := "011011";
	constant CNTRL :std_logic_vector (5 downto 0) := "011100";
	constant AND_r :std_logic_vector (5 downto 0) := "011101";
	constant OR_r :std_logic_vector (5 downto 0) := "011110";
	constant add_c_no_flags :std_logic_vector (5 downto 0) := "011111";
	constant push_pc :std_logic_vector (5 downto 0) := "100000";
	
	constant pop_pc :std_logic_vector (5 downto 0) := "100010";
	constant xor_r :std_logic_vector (5 downto 0) := "100011";
	constant sub_c_no_flags :std_logic_vector (5 downto 0) := "100100";
	constant CMP_c :std_logic_vector (5 downto 0) := "100101";
	constant CLEAR :std_logic_vector (5 downto 0) := "100110";
	constant MOVE :std_logic_vector (5 downto 0) := "100111";
	constant PUT :std_logic_vector (5 downto 0) := "101000";
	constant GET :std_logic_vector (5 downto 0) := "101001";
	constant put_b :std_logic_vector (5 downto 0) := "101010";
	constant get_b :std_logic_vector (5 downto 0) := "101011";
	constant store_b :std_logic_vector (5 downto 0) := "101100";
	constant load_b :std_logic_vector (5 downto 0) := "101101";
	constant put_w :std_logic_vector (5 downto 0) := "101110";
	constant get_w :std_logic_vector (5 downto 0) := "101111";
	constant store_w :std_logic_vector (5 downto 0) := "110000";
	constant load_w :std_logic_vector (5 downto 0) := "110001";	
	constant JMP_r :std_logic_vector (5 downto 0) := "110010";
	constant joz_r :std_logic_vector (5 downto 0) := "110011";
	constant Jnz_r :std_logic_vector (5 downto 0) := "110100";
	constant Joc_r :std_logic_vector (5 downto 0) := "110101";
	constant Jnc_r :std_logic_vector (5 downto 0) := "110110";
	constant Jon_r :std_logic_vector (5 downto 0) := "110111";
	constant Jnn_r :std_logic_vector (5 downto 0) := "111000";
	constant Jov_r :std_logic_vector (5 downto 0) := "111001";
	constant Jnv_r :std_logic_vector (5 downto 0) := "111011";
	constant Joiur_r :std_logic_vector (5 downto 0) := "111100";
	constant Jniur_r :std_logic_vector (5 downto 0) := "111101";
	constant Joour_r :std_logic_vector (5 downto 0) := "111110";
	constant Jnour_r :std_logic_vector (5 downto 0) := "111111";

begin

  steg1_comp : steg1 port map(
    -- IN
    clk => clk,
    instruction_in => instruction_to_steg1,
    pc_in_steg1 => pc_current,
    
    -- UT

    instruction_out => ir1_to_ir2,
    constant_to_steg2 => constant_from_steg1_to_steg2,
    address_to_regs1 => address_to_regs1,
    address_to_regs2 => address_to_regs2,
    pc_out_steg1 => pc_from_steg1,
    joystick_enable => controll_joystick
    );
  
  steg2_comp : steg2 port map(
    -- IN
    clk => clk,	
		rst => reset,
    OP_in=>ir1_to_ir2(31 downto 26),
		areg_in=>ir1_to_ir2(25 downto 21),
		breg_in=>ir1_to_ir2(20 downto 16),
		dreg_in=>ir1_to_ir2(4 downto 0),
    constant_to_im2 => constant_from_steg1_to_steg2,
    regs_data_in1 => data_from_regs1,
    regs_data_in2 => data_from_regs2,
		destination_register_ir3 => dreg3_to_4,
		destination_register_ir4=> address_to_regs3_buff,
		OP_ir3 =>OP3_to_4,
		OP_ir4 => OP_ir4,   
		iur=>iur,
		our=>our,
    dataforward_from_steg3 => dataforward_from_steg3,
    dataforward_from_steg4 => dataforward_from_steg4,
		pc_from_steg1 =>pc_from_steg1,
		reg0_in=>reg0_in,
		internal_nop=>internal_nop,

    -- UT
		address_to_instruction_memory=>address_to_instruction_memory,
		OP_out=>OP2_to_3,
		pc_current_out=>pc_current,
		dreg_out=>dreg2_to_3,
		breg_out=>breg2_to_3,
		constant_out=>constant2_to_3,
    alu_to_steg3 => alu_to_d3
    );

  
  steg3_comp : steg3 port map(
    -- IN
    clk => clk,
    OP_in => OP2_to_3,
		OP_ir4_in=>OP_ir4,
		breg_in=>breg2_to_3,
		dreg_in=>dreg2_to_3,
    data_from_steg2_alu => alu_to_d3,
		dataforwarded_address_steg4=>dataforward_from_steg4(15 downto 0),
		constant_in=>constant2_to_3,
		dreg_steg4_in=>address_to_regs3_buff,

    -- UT
    dreg_out=>dreg3_to_4,
    current_OP => OP3_to_4,
    data_to_steg4 => d3_to_d4,
		write_enable_data_memory => write_enable_to_data_memory,
		data_to_data_memory => data_to_data_memory,
    address_to_data_memory => address_to_data_memory,
		mode_out => mode_out,
    dataforward_steg3 => dataforward_from_steg3
    );
  
  steg4_comp : steg4 port map(
    -- IN
    clk => clk,    
		OP_in=>OP3_to_4,
		dreg_in=>dreg3_to_4,
    data_from_data_memory => data_from_data_memory,
    data_from_steg3_to_d4 => d3_to_d4,
    -- UT
    address_to_regs3 => address_to_regs3_buff,
    data_to_regs => data_from_steg4,
    write_enable_to_regs => write_enable_to_regs,
    current_OP => OP_ir4,
    dataforward_steg4 => dataforward_from_steg4
    );
	address_to_regs3<=address_to_regs3_buff;
-- S�tter data till registren till det som kommer ur steg4
  data_to_regs <= data_from_steg4;

  
-- MUX TILL IR1
-- V�ljer vilken instruktion som skall skickas till IR1.
  instruction_to_steg1<="00000000000000000000000000000000" when reset='1' or internal_nop='1' else
													instruction_from_instruction_memory;
	 
  -- Skicka in en NOP i steg ett om det st�r en JMP
  with ir1_to_ir2(31 downto 26) select
    internal_nop_jmp_steg1 <= '1' when JMP|JOZ|JNZ|JOC|JNC|JON|JNN|JOiur|JNiur|JOour|JNour|
																				JMP_r|JOZ_r|JNZ_r|JOC_r|JNC_r|JON_r|JNN_r|JOiur_r|JNiur_r|JOour_r|JNour_r,
    '0' when others;
  internal_nop <= internal_nop_jmp_steg1;

end cpu_arc;