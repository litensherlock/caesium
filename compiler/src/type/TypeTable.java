package type;

import java.util.HashMap;

public class TypeTable {
    HashMap<String, CType> _pointerTypes;
    HashMap<String, CType> _types;

    CType getType(String name) {
        return _types.get(name);
    }

    CType getPointerType(String name) {
        return _pointerTypes.get(name);
    }





}
