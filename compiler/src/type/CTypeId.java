package type;

public class CTypeId {
    public final static int VOID = 0;

    public final static int U_BYTE = 1;
    public final static int U_SHORT = 2;
    public final static int U_INT = 3;

    public final static int S_BYTE = 4;
    public final static int S_SHORT = 5;
    public final static int S_INT = 6;

    public final static int POINTER = 7;

    private static int _typeId = 8;
    public static int getNewTypeId() {
        return _typeId++;
    }
}
