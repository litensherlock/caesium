package allocators;

import type.CType;

public class MemoryReference {
    private final int _address;
    private final int _size;

    protected MemoryReference(MemoryReference mr) {
        _address = mr._address;
        _size = mr._size;
    }

    public MemoryReference(int address, int size) {
        _address = address;
        _size = size;
    }

    public int getAddress() {
        return _address;
    }

    public int getSize() {
        return _size;
    }

    public void set(byte v) {}
    public void set(short v) {}
    public void set(int v) {}
    public void set(byte[] v) {}

    public void get(CType t) {}
    public void get(short v) {}
    public void get(int v) {}
    public void get(byte[] v) {}
}
