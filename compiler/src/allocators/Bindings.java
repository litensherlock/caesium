package allocators;

import java.util.HashMap;
import java.util.LinkedList;

public class Bindings {
    LinkedList<HashMap<String, SymbolReference>> _scopes;

    public Bindings(StackAllocator allocator) {
        _scopes = new LinkedList<HashMap<String, SymbolReference>>();
    }

    public SymbolReference lookup(String name) {
        SymbolReference sr;
        for (HashMap<String, SymbolReference> scope : _scopes) {
            if ((sr = scope.get(name)) != null) {
                return sr;
            }
        }

        return null;
    }

    public void pushFrame() {
        _scopes.push(new HashMap<String, SymbolReference>());
    }

    public void popFrame() {
        _scopes.pop();
    }
}
