package allocators;

public class SymbolReference extends MemoryReference {
    private final String _name;

    public SymbolReference(String name, MemoryReference mr) {
        super(mr);
        _name = name;
    }

    public String getName() {
        return _name;
    }
}
