package allocators;

import type.CType;

public class StackAllocator {
    private final int _startAddress;
    private int _top;

    public StackAllocator(int startAddress) {
        _startAddress = startAddress;
        _top = startAddress;
    }

    public MemoryReference allocate(CType type, int count) {
        int size = count * type.getSizeOf();
        _top -= size;

        return new MemoryReference(_top, size);
    }

    public void deallocate(MemoryReference reference) {
        if (reference.getAddress() == _top) {
            _top += reference.getSize();
        }
    }
}
