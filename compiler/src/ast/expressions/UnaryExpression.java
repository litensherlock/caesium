package ast.expressions;

import ast.Expression;

public class UnaryExpression extends Expression {
    private final Expression _expr;

    public UnaryExpression(Expression expr) {
        _expr = expr;
    }

    public Expression getExpression() {
        return _expr;
    }
}
