package ast.expressions.arithmetics;

import ast.Expression;
import ast.expressions.BinaryExpression;

public class AdditionExpression extends BinaryExpression {
    public AdditionExpression(Expression left, Expression right) {
        super(left, right);
    }
}
