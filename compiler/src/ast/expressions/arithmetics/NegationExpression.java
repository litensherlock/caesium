package ast.expressions.arithmetics;

import ast.Expression;
import ast.expressions.UnaryExpression;


public class NegationExpression extends UnaryExpression {
    public NegationExpression(Expression expr) {
        super(expr);
    }

}
