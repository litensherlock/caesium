package ast.expressions.arithmetics;

import ast.Expression;
import ast.expressions.BinaryExpression;

public class DivisionExpression extends BinaryExpression {
    public DivisionExpression(Expression left, Expression right) {
        super(left, right);
    }
}
