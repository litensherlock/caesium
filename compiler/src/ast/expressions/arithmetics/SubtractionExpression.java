package ast.expressions.arithmetics;

import ast.Expression;
import ast.expressions.BinaryExpression;

public class SubtractionExpression extends BinaryExpression {
    public SubtractionExpression(Expression left, Expression right) {
        super(left, right);
    }
}
