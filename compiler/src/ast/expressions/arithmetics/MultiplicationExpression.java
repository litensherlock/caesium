package ast.expressions.arithmetics;

import ast.Expression;
import ast.expressions.BinaryExpression;

public class MultiplicationExpression extends BinaryExpression {
    public MultiplicationExpression(Expression left, Expression right) {
        super(left, right);
    }
}
