package ast.expressions.bitwise;

import ast.Expression;
import ast.expressions.BinaryExpression;

public class BitwiseXorExpression extends BinaryExpression {
    public BitwiseXorExpression(Expression left, Expression right) {
        super(left, right);
    }
}
