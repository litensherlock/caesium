package ast.expressions.bitwise;

import ast.Expression;
import ast.expressions.BinaryExpression;

public class RightShiftExpression extends BinaryExpression {
    public RightShiftExpression(Expression left, Expression right) {
        super(left, right);
    }
}
