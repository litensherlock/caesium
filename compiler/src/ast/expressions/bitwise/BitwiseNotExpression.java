package ast.expressions.bitwise;

import ast.Expression;
import ast.expressions.UnaryExpression;

public class BitwiseNotExpression extends UnaryExpression {
    public BitwiseNotExpression(Expression expr) {
        super(expr);
    }
}
