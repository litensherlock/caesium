package ast.expressions.bitwise;

import ast.Expression;
import ast.expressions.BinaryExpression;

public class BitwiseOrExpression extends BinaryExpression {
    public BitwiseOrExpression(Expression left, Expression right) {
        super(left, right);
    }
}
