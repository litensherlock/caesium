package ast.expressions.bitwise;

import ast.Expression;
import ast.expressions.BinaryExpression;

public class BitwiseAndExpression extends BinaryExpression {
    public BitwiseAndExpression(Expression left, Expression right) {
        super(left, right);
    }
}
