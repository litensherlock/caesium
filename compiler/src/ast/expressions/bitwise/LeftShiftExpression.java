package ast.expressions.bitwise;

import ast.Expression;
import ast.expressions.BinaryExpression;

public class LeftShiftExpression extends BinaryExpression {
    public LeftShiftExpression(Expression left, Expression right) {
        super(left, right);
    }
}
