package ast.expressions;

import ast.Expression;

public class BinaryExpression extends Expression {
    private final Expression _left;
    private final Expression _right;

    public BinaryExpression(Expression left, Expression right) {
        _left = left;
        _right = right;
    }

    public Expression getLeftExpression() { return _left; }
    public Expression getRightExpression() { return _right; }
}
