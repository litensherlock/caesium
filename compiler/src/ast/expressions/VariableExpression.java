package ast.expressions;

import ast.Expression;
import type.CType;

public class VariableExpression extends Expression {
    private final CType _type;
    private final String _name;

    public VariableExpression(CType type, String name) {
        _type = type;
        _name = name;
    }

    public CType getType() {
        return _type;
    }

    public String getName() {
        return _name;
    }
}
