package ast.expressions;

import ast.Expression;

public class PrefixExpression extends UnaryExpression {
    public PrefixExpression(Expression expr) {
        super(expr);
    }
}
