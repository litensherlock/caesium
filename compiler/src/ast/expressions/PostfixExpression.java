package ast.expressions;

import ast.Expression;

public class PostfixExpression extends UnaryExpression {
    public PostfixExpression(Expression expr) {
        super(expr);
    }
}
