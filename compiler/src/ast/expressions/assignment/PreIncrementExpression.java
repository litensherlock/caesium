package ast.expressions.assignment;

import ast.Expression;
import ast.expressions.PrefixExpression;

public class PreIncrementExpression extends PrefixExpression {
    public PreIncrementExpression(Expression expr) {
        super(expr);
    }
}
