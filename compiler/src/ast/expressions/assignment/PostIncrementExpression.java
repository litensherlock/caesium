package ast.expressions.assignment;

import ast.Expression;
import ast.expressions.PostfixExpression;

public class PostIncrementExpression extends PostfixExpression {
    public PostIncrementExpression(Expression expr) {
        super(expr);
    }
}
