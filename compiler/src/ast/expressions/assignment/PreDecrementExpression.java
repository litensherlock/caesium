package ast.expressions.assignment;

import ast.Expression;
import ast.expressions.PrefixExpression;

public class PreDecrementExpression extends PrefixExpression {
    public PreDecrementExpression(Expression expr) {
        super(expr);
    }
}
