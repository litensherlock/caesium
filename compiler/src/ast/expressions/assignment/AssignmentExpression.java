package ast.expressions.assignment;

import ast.Expression;
import ast.expressions.BinaryExpression;

public class AssignmentExpression extends BinaryExpression {
    public AssignmentExpression(Expression left, Expression right) {
        super(left, right);
    }
}
