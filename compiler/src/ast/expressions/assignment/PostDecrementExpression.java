package ast.expressions.assignment;

import ast.Expression;
import ast.expressions.PostfixExpression;

public class PostDecrementExpression extends PostfixExpression {
    public PostDecrementExpression(Expression expr) {
        super(expr);
    }
}
