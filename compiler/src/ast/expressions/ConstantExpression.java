package ast.expressions;

import ast.Expression;
import type.CType;

public class ConstantExpression extends Expression {
    private final CType _type;
    private final byte[] _value;

    public ConstantExpression(CType type, byte[] value) {
        _type = type;
        _value = value;
    }

    public CType getType() {
        return _type;
    }

    public byte[] getValue() {
        return _value;
    }
}
