package ast.expressions.referential;

import ast.Expression;
import ast.expressions.BinaryExpression;

public class MemberExpression extends BinaryExpression {
    public MemberExpression(Expression left, Expression right) {
        super(left, right);
    }
}
