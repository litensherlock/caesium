package ast.expressions.referential;

import ast.Expression;
import ast.expressions.BinaryExpression;

public class IndexExpression extends BinaryExpression {
    public IndexExpression(Expression left, Expression index) {
        super(left, index);
    }
}
