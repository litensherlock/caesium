package ast.expressions.referential;

import ast.Expression;
import ast.expressions.BinaryExpression;

public class DereferencedMemberExpression extends BinaryExpression {
    public DereferencedMemberExpression(Expression left, Expression right) {
        super(left, right);
    }
}
