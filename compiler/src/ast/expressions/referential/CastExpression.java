package ast.expressions.referential;

import ast.Expression;
import ast.expressions.UnaryExpression;
import type.CType;

public class CastExpression extends UnaryExpression {
    private final CType _type;

    public CastExpression(CType type, Expression expr) {
        super(expr);
        _type = type;
    }

    public CType getType() {
        return _type;
    }
}
