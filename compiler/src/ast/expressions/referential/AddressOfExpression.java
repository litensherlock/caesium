package ast.expressions.referential;

import ast.Expression;
import ast.expressions.UnaryExpression;

public class AddressOfExpression extends UnaryExpression {
    public AddressOfExpression(Expression expr) {
        super(expr);
    }
}
