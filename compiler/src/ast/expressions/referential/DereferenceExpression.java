package ast.expressions.referential;

import ast.Expression;
import ast.expressions.UnaryExpression;

public class DereferenceExpression extends UnaryExpression {
    public DereferenceExpression(Expression expr) {
        super(expr);
    }
}
