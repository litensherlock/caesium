package ast.expressions.logical;

import ast.Expression;
import ast.expressions.BinaryExpression;

public class LessThanExpression extends BinaryExpression {
    public LessThanExpression(Expression left, Expression right) {
        super(left, right);
    }
}
