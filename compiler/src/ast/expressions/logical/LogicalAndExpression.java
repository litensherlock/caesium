package ast.expressions.logical;

import ast.Expression;
import ast.expressions.BinaryExpression;

public class LogicalAndExpression extends BinaryExpression {
    public LogicalAndExpression(Expression left, Expression right) {
        super(left, right);
    }
}
