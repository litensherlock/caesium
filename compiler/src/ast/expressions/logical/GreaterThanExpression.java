package ast.expressions.logical;

import ast.Expression;
import ast.expressions.BinaryExpression;

public class GreaterThanExpression extends BinaryExpression {
    public GreaterThanExpression(Expression left, Expression right) {
        super(left, right);
    }
}
