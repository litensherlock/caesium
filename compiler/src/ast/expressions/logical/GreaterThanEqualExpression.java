package ast.expressions.logical;

import ast.Expression;
import ast.expressions.BinaryExpression;

public class GreaterThanEqualExpression extends BinaryExpression {
    public GreaterThanEqualExpression(Expression left, Expression right) {
        super(left, right);
    }
}
