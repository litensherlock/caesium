package ast.expressions.logical;

import ast.Expression;
import ast.expressions.BinaryExpression;

public class NotEqualExpression extends BinaryExpression {
    public NotEqualExpression(Expression left, Expression right) {
        super(left, right);
    }
}
