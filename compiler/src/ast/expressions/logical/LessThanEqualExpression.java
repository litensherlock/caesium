package ast.expressions.logical;

import ast.Expression;
import ast.expressions.BinaryExpression;

public class LessThanEqualExpression extends BinaryExpression {
    public LessThanEqualExpression(Expression left, Expression right) {
        super(left, right);
    }
}
