package ast.expressions.logical;

import ast.Expression;
import ast.expressions.BinaryExpression;

public class EqualExpression extends BinaryExpression {
    public EqualExpression(Expression left, Expression right) {
        super(left, right);
    }
}
