package ast.expressions.logical;

import ast.Expression;
import ast.expressions.BinaryExpression;

public class LogicalXorExpression extends BinaryExpression {
    public LogicalXorExpression(Expression left, Expression right) {
        super(left, right);
    }
}
