package ast.expressions.logical;

import ast.Expression;
import ast.expressions.BinaryExpression;

public class LogicalOrExpression extends BinaryExpression {
    public LogicalOrExpression(Expression left, Expression right) {
        super(left, right);
    }
}
