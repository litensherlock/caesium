package ast.expressions.logical;

import ast.Expression;
import ast.expressions.UnaryExpression;

public class LogicalNotExpression extends UnaryExpression {
    public LogicalNotExpression(Expression expr) {
        super(expr);
    }
}
