package ast.statements.structural;

import ast.Expression;
import ast.Statement;

public class LoopStatement extends Statement {
    private final Expression _condition;
    private final Statement _body;

    public LoopStatement(Expression condition, Statement body) {
        _condition = condition;
        _body = body;
    }

    public Expression getCondition() {
        return _condition;
    }

    public Statement getBody() {
        return _body;
    }
}


