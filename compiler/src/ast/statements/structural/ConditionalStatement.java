package ast.statements.structural;

import ast.Expression;
import ast.Statement;

public class ConditionalStatement extends Statement {
    private final Expression _condition;
    private final Statement _consequent;
    private final Statement _alternative;

    public ConditionalStatement(Expression condition, Statement consequent, Statement alternative) {
        _condition = condition;
        _consequent = consequent;
        _alternative = alternative;
    }

    public Expression getCondition() {
        return _condition;
    }

    public Statement getConsequent() {
        return _consequent;
    }

    public Statement getAlternative() {
        return _alternative;
    }
}
