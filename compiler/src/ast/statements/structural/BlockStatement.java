package ast.statements.structural;

import ast.Statement;

public class BlockStatement extends Statement {
    private final Statement[] _statements;

    public BlockStatement(Statement[] statements) {
        _statements = statements;
    }

    public Statement[] getStatements() {
        return _statements;
    }
}
