package ast.statements.declarations;

import type.CType;

public class ArrayDeclarationStatement extends DeclarationStatement {
    private final int _count;

    public ArrayDeclarationStatement(CType type, String name, int count) {
        super(type, name);
        _count = count;
    }

    public int getCount() {
        return _count;
    }
}
