package ast.statements.declarations;

import ast.Statement;
import ast.statements.declarations.DeclarationStatement;

public class UnionStatement extends Statement {
    private final String _name;
    private final DeclarationStatement _fields;

    public UnionStatement(String name, DeclarationStatement fields) {
        _name = name;
        _fields = fields;
    }

    public String getName() {
        return _name;
    }

    public DeclarationStatement getFields() {
        return _fields;
    }
}
