package ast.statements.declarations;

import type.CType;

public class FunctionDeclarationStatement extends DeclarationStatement {
    private final DeclarationStatement _parameters;

    public FunctionDeclarationStatement(CType returnType, String name, DeclarationStatement parameters) {
        super(returnType, name);
        _parameters = parameters;
    }

    public DeclarationStatement getParameters() {
        return _parameters;
    }
}
