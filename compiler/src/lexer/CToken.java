package lexer;

public class CToken extends Token {
    public CToken(int id, String token, int start) {
        super(id, token, start);
    }

    public byte toByte() {
        return (byte) asNumber(_token, 8);
    }

    public short toShort() {
        return (short) asNumber(_token, 16);
    }

    public int toInt() {
        return (int) asNumber(_token, 32);
    }

    public long toLong() {
        return asNumber(_token, 64);
    }

    private long asNumber(String tok, int bits) {
        String hexPrefix = "0x";
        String octPrefix = "0o";
        String binPrefix = "0b";

        long i;
        if (tok.startsWith(hexPrefix)) {
            i = Long.parseLong(tok.substring(hexPrefix.length()), 16);
        } else if (tok.startsWith(octPrefix)) {
            i = Long.parseLong(tok.substring(octPrefix.length()), 8);
        } else if (tok.startsWith(binPrefix)) {
            i = Long.parseLong(tok.substring(binPrefix.length()), 2);
        } else {
            i = Long.parseLong(tok, 10);
        }

        i |= (i & 0x8000000000000000L) >>> (64 - bits);
        return i & (0xffffffffffffffffL >> (64 - bits));
    }
}
