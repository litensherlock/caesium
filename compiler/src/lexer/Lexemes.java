package lexer;

import java.lang.reflect.Field;

public class Lexemes {
    public final static int
        Whitespace = 0,
        Identifier = 1,
        Integer = 2,
        Comma = 3,
        Linebreak = 4,
        Period = 5,
        LParen = 6,
        RParen = 7,
        Register = 8,
        Comment = 9,
        Equals = 10,
        CommentOpen = 11,
        CommentClose = 12,
        Keyword = 13,

        Plus = 14,
        Minus = 15,
        Multiply = 16,
        Divide = 17,
        Modulo = 18,
        Power = 19,

        LBrace = 20,
        RBrace = 21,
        LBracket = 22,
        RBracket = 23,
        LAngle = 24,
        RAngle = 25,

        EndOfStatement = 26,

        Relational = 27,
        Arrow = 28,
        Ampersand = 29,
        Logical = 30,
        Terminal = 31,
        Increment = 32,
        Decrement = 33,
        Circumflex = 34,
        String = 35;

    public static String toString(int lexemeId) {
        for (Field f : Lexemes.class.getFields()){
            try {
                if (f.getInt(Lexemes.class) == lexemeId) {
                    return f.getName();
                }
            } catch (IllegalAccessException e) { }
        }

        return "<unknown lexeme #" + lexemeId + ">";
    }

}
