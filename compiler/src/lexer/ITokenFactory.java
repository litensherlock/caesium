package lexer;

public interface ITokenFactory<T extends Token> {
    T create(int id, String attribute, int start);
}
