package lexer;

public class CTokenFactory implements ITokenFactory<CToken> {
    @Override
    public CToken create(int id, String attribute, int start) {
        return new CToken(id, attribute, start);
    }
}
