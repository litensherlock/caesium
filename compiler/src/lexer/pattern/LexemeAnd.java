package lexer.pattern;

import lexer.LexerReader;

public class LexemeAnd extends LexemeNonterminal {
    private final LexemeNode[] _a;

    public LexemeAnd(LexemeNode... a) {
        _a = a;
    }

    @Override
    public Match match(LexerReader r) {
        Match next = null;
        int ret = 0;

        r.mark();

        int i = 0;
        while (i < _a.length && (next = _a[i++].match(r)).isMatch) {
            ret += next.length;
        }

        boolean succ = next != null && next.isMatch;

        if (succ) r.discardMark();
        else r.recallMark();

        return new Match(succ, ret);
    }
}
