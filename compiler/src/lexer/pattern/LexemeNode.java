package lexer.pattern;

import lexer.LexerReader;

public abstract class LexemeNode {
    public abstract Match match(LexerReader r);

    public static LexemeNode regex(String regex) {
        return new LexemeRegexTerminal(regex);
    }

    public static LexemeNode constant(String constant) {
        return new LexemeConstantTerminal(constant);
    }

    public LexemeNode then(LexemeNode n) {
        return new LexemeAnd(this, n);
    }

    public LexemeNode then(String terminal) {
        return new LexemeAnd(this, regex(terminal));
    }

    public LexemeNode or(LexemeNode n) {
        return new LexemeOr(this, n);
    }

    public LexemeNode or(String terminal) {
        return new LexemeOr(this, regex(terminal));
    }

    public LexemeNode star() {
        return new LexemeStar(this);
    }

    public LexemeNode plus() {
        return new LexemePlus(this);
    }

//
//
//
//    public LexemeNode parse(String str) {
//        LexemeNode digit = new LexemeOr(new LexemeRegexTerminal("\\$(-|\\+)?[\\dA-Fa-f]"), new LexemeRegexTerminal("(-|\\+)?\\d"));
//
//        LexemeNode chr = new LexemeRegexTerminal("[\\p{Alpha}_]");
//        LexemeNode ident = new LexemeAnd(chr, new LexemeStar(new LexemeOr(chr, digit)));
//
//        LexemeNode stringChr = new LexemeOr(new LexemeRegexTerminal("(?:\\\\['tnr]"), new LexemeRegexTerminal("[^'\\x09\\x0a\\x0d])"));
//        LexemeNode regex = new LexemeAnd(new LexemeRegexTerminal("'"), new LexemeStar(stringChr), new LexemeRegexTerminal("'"));
//
//        Lexer<LexemeToken> lexer = new Lexer<>(
//                str,
//                new LexemeTokenFactory(),
//                new Lexeme[] {
//                        new Lexeme(new LexemeRegexTerminal("[\\x20\t\r]+"), Lexemes.Whitespace),
//                        new Lexeme(new LexemeRegexTerminal("\\n"), Lexemes.Linebreak),
//                        new Lexeme(ident, Lexemes.Identifier),
//                        new Lexeme(regex, Lexemes.Terminal),
//                        new Lexeme(new LexemeRegexTerminal(";"), Lexemes.EndOfStatement),
//                        new Lexeme(new LexemeRegexTerminal(":="), Lexemes.Equals),
//                        new Lexeme(new LexemeRegexTerminal("\\*"), Lexemes.Multiply),
//                        new Lexeme(new LexemeRegexTerminal("\\+"), Lexemes.Plus),
//                        new Lexeme(new LexemeRegexTerminal("\\("), Lexemes.LParen),
//                        new Lexeme(new LexemeRegexTerminal("\\)"), Lexemes.RParen),
//                });
//
//
//    }
}
