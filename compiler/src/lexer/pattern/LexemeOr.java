package lexer.pattern;

import lexer.LexerReader;

public class LexemeOr extends LexemeNonterminal {
    private final LexemeNode _a;
    private final LexemeNode _b;

    public LexemeOr(LexemeNode a, LexemeNode b) {
        _a = a;
        _b = b;
    }

    @Override
    public Match match(LexerReader r) {
        Match next;
        int ret = 0;

        r.mark();
        if ((next = _a.match(r)).isMatch) {
            r.discardMark();
            return next;
        }
        r.recallMark();

        return _b.match(r);
    }
}
