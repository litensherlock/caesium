package lexer.pattern;

import lexer.LexerReader;

public class LexemeStar extends LexemeNonterminal {
    private final LexemeNode _node;

    public LexemeStar(LexemeNode node) {
        _node = node;
    }

    @Override
    public Match match(LexerReader r) {
        Match next;
        int ret = 0;

        while ((next = _node.match(r)).isMatch) {
            ret += next.length;
        }

        return new Match(true, ret);
    }
}
