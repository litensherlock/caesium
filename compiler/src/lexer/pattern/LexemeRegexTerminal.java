package lexer.pattern;

import lexer.LexerReader;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LexemeRegexTerminal extends LexemeNode {
    private final Pattern _rgx;

    public LexemeRegexTerminal(String regex) {
        _rgx = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
    }

    @Override
    public Match match(LexerReader reader) {
        Matcher m = _rgx.matcher(reader);

        if (m.lookingAt()) {
            //String tok = m.group();
            //destination.append(tok);
            reader.advance(m.end());

            return new Match(true, m.end());
        }

        return new Match(false, 0);
    }
}
