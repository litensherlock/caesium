package lexer.pattern;

public class Match {
    public final boolean isMatch;
    public final int length;

    public Match(boolean isMatch, int length) {
        this.isMatch = isMatch;
        this.length = length;
    }
}
