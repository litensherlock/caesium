package lexer;

import java.util.regex.Pattern;

public class LexemePattern {
    private final Pattern _pattern;
    private final int _symbol;

    public LexemePattern(String pattern, int symbol) {
        _pattern = Pattern.compile(pattern);
        _symbol = symbol;
    }

    public Pattern getPattern() { return _pattern; }
    public int getSymbolId() { return _symbol; }
}
