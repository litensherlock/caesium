package lexer;

public class FunctionToken extends Token {
    private int _argc;

    public FunctionToken(Token id) {
        this(id, 0);
    }

    public FunctionToken(Token id, int argc) {
        super(id);
        _argc = argc;
    }

    public int getArgCount() {
        return _argc;
    }

    public void incArgCount() {
        _argc++;
    }
}
