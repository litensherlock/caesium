package lexer;

import lexer.pattern.LexemeNode;

public class Lexeme {
    private final LexemeNode _node;
    private final int _symbol;

    public Lexeme(LexemeNode node, int symbol) {
        _node = node;
        _symbol = symbol;
    }

    public int getSymbolId() { return _symbol; }
    public LexemeNode getNode() { return _node; }
}
