package lexer;

import java.lang.reflect.Field;

public class Keywords {

    public final static String If = "if";
    public final static String Else = "else";
    public final static String Struct = "struct";
    public final static String While = "while";
    public final static String For = "for";
    public final static String Union = "union";


    public static String regex() {
        StringBuilder sb = new StringBuilder();

        for (Field f : Keywords.class.getFields()) {
            try {
                sb.append(f.get(Keywords.class)).append("|");
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }

        sb.setLength(sb.length() - 1);

        return sb.toString();
    }
}
