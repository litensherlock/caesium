package lexer;

public class PairList<T> {
    private LinkPair<T> _head;
    private LinkPair<T> _tail;

    public PairList() {
        
    }
    
    public LinkPair<T> head() {
        return _head;
    }
    
    public LinkPair<T> enqueue(T value) {
        if (_head == null) {
            _head = _tail = new LinkPair<T>(value, null);            
        } else {
            _tail.next = new LinkPair<T>(value, null);
            _tail = _tail.next;
        }

        return _tail;        
    }
    
    public LinkPair<T> dequeue() {
        if (_head == null) {
            return null;
        }
        
        LinkPair<T> ret = _head;
        _head = _head.next;
        
        return ret;
    }
    
    
    
}
