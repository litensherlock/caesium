package lexer;

public class LinkPair<T> {
    public final T value;
    public LinkPair<T> next;

    public LinkPair(T value, LinkPair<T> next) {
        this.value = value;
        this.next = next;
    }
}
