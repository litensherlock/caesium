package parser;

import ast.Expression;
import ast.Statement;
import ast.expressions.arithmetics.AdditionExpression;
import ast.statements.structural.BlockStatement;
import ast.statements.structural.ConditionalStatement;
import ast.statements.structural.LoopStatement;
import ast.statements.structural.NullStatement;
import lexer.CToken;
import lexer.Keywords;
import lexer.Lexemes;

import java.util.ArrayList;

public class CParser {

    private CLexer _lexer;

    public CParser() { }

    public Statement parse(CLexer lexer) {
        _lexer = lexer;

        return parseStatement();
    }

    private Statement parseStatement() {
        Statement ret;
        switch (_lexer.peek().getId()) {
            case Lexemes.LBrace: ret = parseBlock(); break;
            case Lexemes.Keyword: ret = parseKeyword(); break;
            default: ret = parseExpression();  break;
        }

        _lexer.expect(Lexemes.EndOfStatement);

        return ret;
    }

    private Statement parseKeyword() {
        CToken kw = _lexer.peek();

        if (kw.hasAttribute(Keywords.If)) return parseIf();
        else if (kw.hasAttribute(Keywords.While)) return parseWhile();
        else if (kw.hasAttribute(Keywords.For)) return parseFor();

        return new NullStatement();
    }

    private Statement parseFor() {
        Expression init, cond, incr;
        Statement body;

        _lexer.expect(Lexemes.Keyword, Keywords.For);
        _lexer.expect(Lexemes.LParen);

        init = parseExpression();
        _lexer.expect(Lexemes.EndOfStatement);
        cond = parseExpression();
        _lexer.expect(Lexemes.EndOfStatement);
        incr = parseExpression();
        _lexer.expect(Lexemes.RParen);
        body = parseStatement();

        // this is a rewrite to "init; while(cond) { stmt; incr; }"

        Statement[] block;
        if (body instanceof BlockStatement) {
            // if the body is already a block,
            // add the incrementor to the end
            block = new Statement[((BlockStatement) body).getStatements().length + 1];
            Statement[] statements = ((BlockStatement) body).getStatements();
            for (int i = 0, length = statements.length; i < length; i++) {
                block[i] = statements[i];
            }
        } else {
            // if a single statement, make a block with that
            // statement and the incrementor
            block = new Statement[2];
            block[0] = body;
        }
        block[block.length - 1] = incr;

        Statement[] loop = {
            init,
            new LoopStatement(cond, new BlockStatement(block))
        };

        return new BlockStatement(loop);
    }

    private Statement parseWhile() {
        Expression cond;
        Statement body;

        _lexer.expect(Lexemes.Keyword, Keywords.While);
        _lexer.expect(Lexemes.LParen);
        cond = parseExpression();
        _lexer.expect(Lexemes.RParen);
        body = parseStatement();

        return new LoopStatement(cond, body);
    }

    private Statement parseIf() {
        _lexer.expect(Lexemes.Keyword, Keywords.If);

        _lexer.expect(Lexemes.LParen);
        Expression cond = parseExpression();
        _lexer.expect(Lexemes.RParen);
        Statement conseq = parseStatement();
        Statement alt = null;

        if (_lexer.acceptToken(Lexemes.Keyword, Keywords.Else)) {
            alt = parseStatement();
        }

        return new ConditionalStatement(cond, conseq, alt);
    }

    private Statement parseBlock() {
        ArrayList<Statement> smts = new ArrayList<>();

        _lexer.expect(Lexemes.LBrace);
        while (!_lexer.peek(Lexemes.RBrace)) {
            smts.add(parseStatement());
        }
        _lexer.expect(Lexemes.RBrace);

        Statement[] ret = new Statement[smts.size()];
        smts.toArray(ret);
        return new BlockStatement(ret);
    }

    private Expression parseExpression() {
        // Implement shunting yard alg
        return new AdditionExpression(null, null);
    }
}
