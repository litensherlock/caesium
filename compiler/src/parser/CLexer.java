package parser;

import lexer.*;
import lexer.pattern.LexemeNode;

public class CLexer extends Lexer<CToken> {
    private static LexemeNode
            ws = LexemeNode.regex("[\\x20\t\r]+"),
            digitBin = LexemeNode.regex("[01]"),
            digitOct = LexemeNode.regex("[234567]").or(digitBin),
            digitDec = LexemeNode.regex("[89]").or(digitOct),
            digitHex = LexemeNode.regex("[a-f]").or(digitDec),
            number = LexemeNode.constant("0b").then(digitBin.plus())
                    .or(LexemeNode.constant("0o").then(digitOct.plus()))
                    .or(LexemeNode.constant("0x").then(digitHex.plus()))
                    .or(digitDec.plus()),
            character = LexemeNode.regex("[\\p{Alpha}_]"),
            identifier = character.then(character.or(digitDec).star()),
            keywords = LexemeNode.regex(Keywords.regex()),
            string = LexemeNode.constant("\"").then("(\\\\[\\\"tnr]|[^\\\"\\x09\\x0a\\x0d])+").then(LexemeNode.constant("\""));

    public CLexer(String text) {
        super(
                text,
                new CTokenFactory(),
                new Lexeme[] {
                        new Lexeme(ws, Lexemes.Whitespace),
                        new Lexeme(string, Lexemes.String),
                        new Lexeme(keywords, Lexemes.Keyword),
                        new Lexeme(identifier, Lexemes.Identifier),
                        new Lexeme(number, Lexemes.Integer),

                        new Lexeme(LexemeNode.regex("\\<|\\>|\\<=|\\>=|==|!="), Lexemes.Relational),
                        new Lexeme(LexemeNode.regex("!|&&|\\|\\|\\^\\^"), Lexemes.Logical),
                        new Lexeme(LexemeNode.constant("->"), Lexemes.Arrow),
                        new Lexeme(LexemeNode.constant("//"), Lexemes.Comment),
                        new Lexeme(LexemeNode.constant("/*"), Lexemes.CommentOpen),
                        new Lexeme(LexemeNode.constant("*/"), Lexemes.CommentClose),
                        new Lexeme(LexemeNode.constant("++"), Lexemes.Increment),
                        new Lexeme(LexemeNode.constant("--"), Lexemes.Decrement),

                        new Lexeme(LexemeNode.constant(","), Lexemes.Comma),
                        new Lexeme(LexemeNode.constant("."), Lexemes.Period),
                        new Lexeme(LexemeNode.constant("="), Lexemes.Equals),
                        new Lexeme(LexemeNode.constant("\n"), Lexemes.Linebreak),
                        new Lexeme(LexemeNode.constant("("), Lexemes.LParen),
                        new Lexeme(LexemeNode.constant(")"), Lexemes.RParen),
                        new Lexeme(LexemeNode.constant("{"), Lexemes.LBrace),
                        new Lexeme(LexemeNode.constant("}"), Lexemes.RBrace),
                        new Lexeme(LexemeNode.constant("["), Lexemes.LBracket),
                        new Lexeme(LexemeNode.constant("]"), Lexemes.RBracket),
                        new Lexeme(LexemeNode.constant(";"), Lexemes.EndOfStatement),
                        new Lexeme(LexemeNode.constant("+"), Lexemes.Plus),
                        new Lexeme(LexemeNode.constant("-"), Lexemes.Minus),
                        new Lexeme(LexemeNode.constant("%"), Lexemes.Modulo),
                        new Lexeme(LexemeNode.constant("*"), Lexemes.Multiply),
                        new Lexeme(LexemeNode.constant("/"), Lexemes.Divide),
                        new Lexeme(LexemeNode.constant("^"), Lexemes.Circumflex),
                        new Lexeme(LexemeNode.constant("&"), Lexemes.Ampersand),
                }
        );

        setLinebreak(Lexemes.Linebreak);
        setWhitespace(Lexemes.Whitespace);
        setCommentLine(Lexemes.Comment);
        setCommentBlock(Lexemes.CommentOpen, Lexemes.CommentClose);
        setIsLinebreakWhiteSpace(true);

    }
}
