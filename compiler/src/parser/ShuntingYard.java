package parser;

import lexer.*;

import java.util.Deque;
import java.util.LinkedList;

public class ShuntingYard {
    public static Deque<Token> ShuntingYard(Lexer lexer) {
        Deque<Token> output = new LinkedList<>();
        Deque<Token> stack = new LinkedList<>();
        Deque<FunctionToken> fnStack = new LinkedList<FunctionToken>();

        // read one token from the input stream
        while(!lexer.peek(Lexemes.Linebreak))   {

            if(lexer.peek(Lexemes.Integer))  {
                // If the token is a number (identifier), then add it to the output queue.
                if (!fnStack.isEmpty()) {
                    fnStack.peek().incArgCount();
                }

                output.add(lexer.next());
            } else if (lexer.peek(Lexemes.Identifier)) {
                // If the token is a function token, then push it onto the stack.
                Token id =  lexer.next();
                if (lexer.peek(Lexemes.LParen)) {
                    FunctionToken fc = new FunctionToken(id);
                    fnStack.push(fc);
                    stack.push(fc);
                } else {
                    // variable / const

                    if (!fnStack.isEmpty()) {
                        fnStack.peek().incArgCount();
                    }

                    output.add(id);
                }
            } else if(lexer.peek(Lexemes.Comma)) {
                // If the token is a function argument separator (e.g., a comma):

                lexer.next();
                while(stack.peek().getId() != Lexemes.LParen)   {
                    // Until the token at the top of the stack is a left parenthesis,
                    // pop operators off the stack onto the output queue.
                    output.add(stack.pop());
                }
            } else if (is_operator(lexer.peek())) {
                // If the token is an operator, op1, then:
                while(!stack.isEmpty()) {
                    Token st = stack.peek();
                    Token lex = lexer.peek();

                    // While there is an operator token, op2, at the top of the stack
                    // op1 is left-associative and its precedence is less than or equal to that of op2,
                    // or op1 has precedence less than that of op2,
                    // Let + and ^ be right associative.
                    // Correct transformation from 1^2+3 is 12^3+
                    // The differing operator priority decides pop / push
                    // If 2 operators have equal priority then associativity decides.
                    if (!is_operator(st) || ((!is_left_assoc(lex) || (precedence(lex) > precedence(st))) && (precedence(lex) >= precedence(st)))) {
                        break;
                    }

                    FunctionToken fl = new FunctionToken(stack.pop(), op_arg_count(st));
                    fnStack.push(fl);
                    output.add(fl);
                }

                // push op1 onto the stack.
                Token lex = lexer.next();
                stack.add(new FunctionToken(lex, op_arg_count(lex)));
            } else if(lexer.peek(Lexemes.LParen))   {
                // If the token is a left parenthesis, then push it onto the stack.
                stack.push(lexer.next());
            } else if(lexer.peek(Lexemes.RParen))    {
                // If the token is a right parenthesis:
                lexer.next();

                // Until the token at the top of the stack is a left parenthesis,
                // pop operators off the stack onto the output queue
                while(stack.peek().getId() != Lexemes.LParen) {
                    output.add(stack.pop());
                    fnStack.peek().incArgCount(); // count number of args
                }

                // If the stack runs out without finding a left parenthesis, then there are mismatched parentheses.
                if(stack.isEmpty())  {
                    System.out.println("Error: parentheses mismatched");
                    return null;
                } else {
                    // Pop the left parenthesis from the stack, but not onto the output queue.
                    stack.pop();

                    // If the token at the top of the stack is a function token, pop it onto the output queue.
                    if (stack.peek().getId() == Lexemes.Identifier) {
                        stack.pop();
                        output.add(fnStack.pop());
                    }
                }
            }
            else  {
                System.out.println("Unknown token " + lexer.peek().getAttribute());
                return null;
            }
        }

        // When there are no more tokens to read:
        // While there are still operator tokens in the stack:
        while(!stack.isEmpty())  {

            Token st = stack.pop();
            if(st.getId() == Lexemes.LParen || st.getId() == Lexemes.RParen) {
                System.out.println("Error: parentheses mismatched");
                return null;
            }
            output.add(st);
        }

        return output;
    }

    private static int op_arg_count(Token l) {
        switch (l.getId()) {
            case Lexemes.Circumflex:
            case Lexemes.Multiply:
            case Lexemes.Divide:
            case Lexemes.Modulo:
            case Lexemes.Plus:
            case Lexemes.Minus:
                return 2;
            default:
                return 0;
        }
    }

    private static boolean is_operator(Token l) {
        switch (l.getId()) {
            case Lexemes.Plus:
            case Lexemes.Minus:
            case Lexemes.Multiply:
            case Lexemes.Divide:
            case Lexemes.Modulo:
            case Lexemes.Circumflex:
                return true;
            default:
                return false;
        }
    }

    private static boolean is_left_assoc(Token l) {
        switch (l.getId()) {
            case Lexemes.Plus:
            case Lexemes.Minus:
            case Lexemes.Multiply:
            case Lexemes.Divide:
            case Lexemes.Modulo:
                return true;
            default:
                return false;
        }
    }

    public static int precedence(Token l) {
        switch (l.getId()) {
            case Lexemes.Circumflex:
                return 1;
            case Lexemes.Multiply:
            case Lexemes.Divide:
            case Lexemes.Modulo:
                return 2;
            case Lexemes.Plus:
            case Lexemes.Minus:
                return 3;
            default:
                return -1;
        }
    }



}
